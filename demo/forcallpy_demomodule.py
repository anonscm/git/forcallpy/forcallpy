# Python module for our test of embedding Python within Fortran program.

#====== Standard basic code test.
import math
import numpy as np
import sys
import os

print("Begin of module forcallpy_testmodule.py initialization")

#print("Running context:")
#print("=========================> sys.path:\n", sys.path)
#print("=========================> os.environ:\n", os.environ)

print("Pi is", math.pi)

for i in range(3):
    print("A python loop", i, "in a module.")

#====== Function to call from fortran with arrays. 
def a_subroutine(itab, ftab, ptab):
    print("Called a_subroutine() with:")
    print("   Integers", itab)
    print("   Floats", ftab)
    print("   Pointers", ptab)
    print("   Types:", type(itab), type(ftab), type(ptab))

#====== Function to call from fortran to modify a param and return a result.
def a_function(a,b,c,x,vint,vfloat,resfloats):
    print("Called a_int_function() with:")
    print("    a,b,c,x:", a, b, c, x)
    print("    vint:", vint)
    print("    vfloat:", vfloat)
    print("    resfloats:", resfloats)
    print("    types vint,vfloat,resfloats:", type(vint), type(vfloat), type(resfloats))
    print("    np.sin of our float array:", np.sin(vfloat))
    print("    Modifying resfloats array with values of int squares")
    for i in range(len(resfloats)):
        resfloats[i] = (i+1) ** 2
    res = a * x**2 + b*x + c
    print("    -> Returning:", res)
    return res

#====== Multithreading tests… 
def test_threading():
    import threading
    import time

    print("Multithreading in Python...")
    def testfct(x):
        print("begin thread", threading.get_ident(), time.time())
        time.sleep(x)
        print("end thread", threading.get_ident(), time.time())
        
    for i in range(2,8,2):
        t = threading.Thread(target=testfct, args=(i,), daemon=True)
        t.start()
        
    print("...Threads started")
    
print("End of module forcallpy_testmodule.py initialization")
