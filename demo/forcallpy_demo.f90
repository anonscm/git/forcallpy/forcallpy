! Start/top functins are:
!   pyinit() subroutine initialize the library internals.
!   pyterm() terminate library usage, free library resources (no longer usable, 
!            cannot be re-initialized).
!
! Available Python call functions/subroutines are:
!   pyrun() subroutine execute a complete script which may contain multiple lines
!           and can modify the global namespace by setting names. These names remain
!           defined from one call to another (and are available in pycall() and pyfct()),
!           they cannot be same as optional names defined below.
!   pycall() subroutine execute one statement.
!   pyfct() function evaluate an expression and return its result.
! These names are postfixed by _i4r8 to specify integer and real sizes (Integer 4 bytes, 
! Real 8 bytes). And by _i4 or _r8 for pyfct() to specify attended result type and size.
!
! When calling forcallpy functions/subroutines, optional parameters are:
!   a...h: Eight integers (currently 4 bytes) [in].
!   s...z: Eight reals (currently 8 bytes) [in].
!   av...cv: Three one dimension arrays of Integers (read only) [in].
!   xv...zv: Three one dimension arrays of Reals (read only) [in].
!   aw...cw: Three one dimension arrays of Integers (read/write) [inout].
!   xw...zw: Three one dimension arrays of Reals (read/write) [inout].
!   p: One one dimension array of c_ptr (read/write) [inout].
!   coderr: Integer for error code (0 if ok) [out].
! These parameters are available with same name as locals when evaluating the
! Python expression. Arrays are directly passed to Python code as numpy.ndarray
! wrapped access to fortran arrays (there is no copy of arrays data).

PROGRAM forcallpy_demo
  USE forcallpy
  USE, INTRINSIC :: iso_c_binding, ONLY: c_ptr, c_null_ptr, c_loc
  
  IMPLICIT NONE
  INTEGER :: coderr
  INTEGER :: intres
  INTEGER,DIMENSION(10),TARGET :: tabint;
  DOUBLE PRECISION,DIMENSION(5),TARGET :: tabdbl;
  TYPE(c_ptr),DIMENSION(3) :: tabptr;
  DOUBLE PRECISION,DIMENSION(4),TARGET :: tabres;
  
  tabint(1:10) = (/ 3, 4, 5, 6, 7, 8, 9, 10, 11, 12  /)
  tabdbl(1:5) = (/ 10.1, 10.3, 10.5, 10.7, 10.9 /)
  tabptr(1:3) = (/ c_null_ptr, c_null_ptr , c_null_ptr  /)
  tabres(1:4) = (/ 0.0, 0.0, 0.0, 0.0 /)
  ! Test to transmit address of an existing array.
  ! This array must have POINTER or TARGET attribute.
  tabptr(2) = c_loc(tabdbl)
  
  ! Initialize the library (start Python interpreter), create the global namespace
  ! (available when evaluating Python code) with available modules __builtins__, 
  ! np (numpy) and forcallpy (tool functions from this library).
  CALL pyinit(register_sighandlers=1, print_errors=1, verbosity=0, coderr=coderr)

  ! -----------------------------------------------------------------------
  ! pyrun allows to run one or several Python statements, eventually on several lines.
  ! It can modify the interpreter globals via setting some variables, importing
  ! some modules, etc.
  
  ! Simple computation, builtin print function call, using a real parameter.
  CALL pyrun_i4r8('print("result =",3 + 4 *  x)', x=3.6D0)

  ! Run a loop statement with prints.
  CALL pyrun_i4r8('for i in range(3): print("This is a Python loop", i, "directly in fortran source.")')

  ! Import a module and use.
  CALL pyrun_i4r8('import math')
  CALL pyrun_i4r8('print("Pi value is", math.pi)')
  
  ! Use our own Python module (file), prepare its import by modifying the
  ! Python path.
  ! This module start threads (which sleep some times).
  ! See its code.
  CALL pyrun_i4r8('import sys'//NEW_LINE('a')//'sys.path.insert(0, ".")')
  CALL pyrun_i4r8('import forcallpy_demomodule')

  ! See if our internal module tool is imported (normally automatic, like numpy as np)
  CALL pyrun_i4r8('print("Doc forcallpy:", forcallpy.__doc__)')
  CALL pyrun_i4r8('print("Namespace forcallpy:", dir(forcallpy)')
  
  ! -----------------------------------------------------------------------  
  ! pyfct evaluate one expression 
  ! Call a function from our python module, using simple named values as parameters,
  ! return an integer.
  
  WRITE(*,*) "Inout array before:", tabres

  intres = pyfct_i4r8_i4("int(forcallpy_demomodule.a_function(a,b,c,x,av,zv,yw))", &
                                    a=2,b=-4,c=7,x=3.5D+0,av=tabint,zv=tabdbl,yw=tabres)
  ! Seem a bad idea to use write with a call to pyfct(), maybe there is some
  ! lock around stdout, but process hang (no coredump, just hang).
  WRITE(*,*) "Result =", intres
  WRITE(*,*) "Inout array after:", tabres

  ! Same, ignoring the result (Python subroutines are simple functions).
  CALL pysub_i4r8("forcallpy_demomodule.a_subroutine(av,zv,p)", &
                                    av=tabint,zv=tabdbl,p=tabptr)      


  ! Print an exception.
  WRITE(*,*) "Computing a zero division in Python"
  CALL pysub_i4r8("print(1/0)")

  ! -----------------------------------------------------------------------  
  ! A final test with three Python thread working during some Fortran code activity
  ! (here activity is sleeping…).
  ! Test multithreaded Python code (at the end, quick-test with crash before waiting with sleep).
  CALL pyrun_i4r8('forcallpy_demomodule.test_threading()')
  ! Sleep some time in our main thread to let Python threads time to finish.
  call sleep(10)

  ! Properly terminate the interpreter.
  CALL pyterm()
  
END PROGRAM forcallpy_demo
