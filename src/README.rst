 forcallpy
 =========
 
 :copyright: (C) 2017 - CNRS/LIMSI
 :author: Laurent Pointal <laurent.pointal@limsi.fr>
 
 Fortran library wrapping an embedded Python 3 + NumPy to allow simple
 call to Python code from Fortran.
 
 Source repository: https://sourcesup.renater.fr/scm/?group_id=3559
 
 Documentation is in the repository as a sphinx ReST files in docs/ directory.
 
 The repository also includes a demo/ directory with a simple example of a 
 Fortran program using this library.
 
 Fortran functions
 -----------------
 
 * pyinit
 * pyterm
 * pyrun_i4r8
 * pysub_i4r8
 * pyfct_i4r8_i4
 
 