! Project: forcallpy
! License: CeCILL-B (BSD like)
!
! Copyright CNRS/LIMSI (2017) by Laurent Pointal
! 
! laurent.pointal@limsi.fr
! 
! This software is a computer program whose purpose is to call Python code
! from Fortran programs..
! 
! This software is governed by the CeCILL-B license under French law 
! and abiding by the rules of distribution of free software.  
! You can  use, modify and/ or redistribute the software under the terms of 
! the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following 
! URL "http://www.cecill.info". 
! 
! As a counterpart to the access to the source code and  rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty  and the software's author,  the holder of the
! economic rights,  and the successive licensors  have only  limited
! liability. 
! 
! In this respect, the user's attention is drawn to the risks associated
! with loading,  using,  modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean  that it is complicated to manipulate,  and  that  also
! therefore means  that it is reserved for developers  and  experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or 
! data to be ensured and,  more generally, to use and operate it in the 
! same conditions as regards security. 
!  
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL-B license and that you accept its terms.
 
MODULE forcallpy
  USE, INTRINSIC :: iso_c_binding, ONLY: c_double,c_ptr,c_loc,c_int32_t
  USE, INTRINSIC :: iso_fortran_env, ONLY : error_unit
  IMPLICIT NONE

  ! Fortran Value parameters for dynamic array allocation.
  ! Must match with C constants.
  ENUM,BIND(c)
    ENUMERATOR :: FCP_INT32 = 1, FCP_INT64 = 2
    ENUMERATOR :: FCP_REAL32 = 3, FCP_REAL64 = 4
  ENDENUM


INTERFACE

  
END INTERFACE      
 
CONTAINS



!!!!!!!!!!!!!!!!!!! ??????????? définir une structure qui empaquette:
! Le type de donnée
! le c_ptr vers le POINTER lié au tableau
! les 3 (max) dimensions du shape
! Et qui soit définie de façon similaire dans le C, ceci de façon à
! ne pas se trimballer 50 paramètres.
! Peut-être faudrait-il avoir à ce moment un paramètre des pyrun & Co
! qui soit *explicitement* un tableau de POINTER (plus propre que
! des c_ptr(truc machin).
! À la limite, on devrait pouvoir avoir du polymorphisme pour la 
! création des éléments de ce tableau (vu qu'on ne stocke qu'un
! c_ptr dedans).
! Mais où seront les données quand on passera des constantes…
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


  
!   SUBROUTINE forcallpy_falloc_i4(itab, ilen)
!     INTEGER(c_int32_t),INTENT(in),VALUE :: exprkind
!     INTEGER,INTENT(in),VALUE :: ilen
! 
!   END SUBROUTINE forcallpy_falloc_i4

  ! Dynamically allocate fortran array in tabptr (POINTER) for dim0 elements.
  ! Returns address of allocated memory usable in C functions.
  FUNCTION forcallpy_ptrallocate1d_r8(tabptr, dim0, coderr) BIND(C,  NAME="c_forcallpy_ptrallocate1d_r8")
    USE, INTRINSIC :: iso_c_binding, ONLY: c_double,c_ptr,c_loc,c_int32_t,c_null_ptr
    IMPLICIT NONE
    ! --- Parameters and return type
    REAL(c_double),DIMENSION(:),INTENT(inout),POINTER :: tabptr
    INTEGER(c_int32_t),INTENT(in),VALUE :: dim0
    INTEGER(c_int32_t),INTENT(out) :: coderr
    TYPE(c_ptr) :: forcallpy_ptrallocate1d_r8
    ! --- Locals
    INTEGER :: status

    ALLOCATE(tabptr(dim0), stat=status)
    IF (status /= 0) THEN
        WRITE(UNIT=error_unit, FMT=*) "Error in Fortran POINTER's ALLOCATE"
        coderr = status;
        ! Return NULL C pointer.
        forcallpy_ptrallocate1d_r8 = c_null_ptr;
    ELSE
        ! To return address of allocated memory (for use by C caller to build an
        ! associated NumPy ndarray, get address of first item.
        forcallpy_ptrallocate1d_r8 = c_loc(tabptr(1))
        coderr = 0;
    ENDIF
  END FUNCTION forcallpy_ptrallocate1d_r8

  ! Dynamically allocate fortran array in tab (ALLOCATABLE) for dim0 elements.
  ! Returns address of allocated memory usable in C functions.
  FUNCTION forcallpy_allocate1d_r8(tab, dim0, coderr) BIND(C,  NAME="c_forcallpy_allocate1d_r8")
    USE, INTRINSIC :: iso_c_binding, ONLY: c_double,c_ptr,c_loc,c_int32_t,c_null_ptr
    IMPLICIT NONE
    ! --- Parameters and return type
    REAL(c_double),DIMENSION(:),INTENT(inout),ALLOCATABLE,TARGET :: tab
    INTEGER(c_int32_t),INTENT(in),VALUE :: dim0
    INTEGER(c_int32_t),INTENT(out) :: coderr
    TYPE(c_ptr) :: forcallpy_allocate1d_r8
    ! --- Locals
    INTEGER :: status

    ALLOCATE(tab(dim0), stat=status)
    IF (status /= 0) THEN
        WRITE(UNIT=error_unit, FMT=*) "Error in Fortran ALLOCATABLE's ALLOCATE"
        coderr = status;
        ! Return NULL C pointer.
        forcallpy_allocate1d_r8 = c_null_ptr;
    ELSE
        ! To return address of allocated memory (for use by C caller to build an
        ! associated NumPy ndarray, get address of first item.
        forcallpy_allocate1d_r8 = c_loc(tab(1))
        coderr = 0;
    ENDIF
  END FUNCTION forcallpy_allocate1d_r8

  ! Dynamically allocate fortran array in tab for dim0 elements.
  SUBROUTINE forcallpy_deallocate1d_r8(tab, coderr) BIND(C,  NAME="c_forcallpy_deallocate1d_r8")
    USE, INTRINSIC :: iso_c_binding, ONLY: c_double,c_ptr,c_loc,c_int32_t
    IMPLICIT NONE
    ! --- Parameters and return type
    REAL(c_double),DIMENSION(:),INTENT(inout),POINTER :: tab
    INTEGER(c_int32_t),OPTIONAL,INTENT(out) :: coderr
    ! --- Locals

    IF (ASSOCIATED(tab)) THEN
        DEALLOCATE(tab)
        NULLIFY(tab)
    ENDIF
    !NULLIFY(tab) ... should be done by deallocate
    
  END SUBROUTINE forcallpy_deallocate1d_r8


  ! Dynamically allocate fortran array in tab for dim0 elements.
  ! Returns address of allocated memory usable in C functions.
!   FUNCTION forcallpy_allocate2d_r8(tab, dim0, dim1) BIND(C)
!     USE, INTRINSIC :: iso_c_binding, ONLY: c_double,c_ptr,c_loc,c_int32_t
!     IMPLICIT NONE
!     ! --- Parameters and return type
!     REAL(c_double),DIMENSION(:,:),INTENT(inout),POINTER,TARGET :: tab
!     INTEGER(c_int32_t),INTENT(in),VALUE :: dim0, dim1
!     TYPE(c_ptr) :: forcallpy_allocate2d_r8
!     ! --- Locals
! 
!     ALLOCATE(tab(dim0,  dim1))
!     ! Problème: est-ce que ça récupère l'adresse du pointeur ou l'adresse de 
!     ! la mémoire allouée…? PB: %REF et %VAL c'est pour les appels de fonctions.
!     forcallpy_allocate2d_r8 = c_loc(tab[1,1])
!   END FUNCTION forcallpy_allocate2d_r8
!   

END MODULE forcallpy

