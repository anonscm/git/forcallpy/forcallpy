! Project: forcallpy
! License: CeCILL-B (BSD like)
!
! Copyright CNRS/LIMSI (2017) by Laurent Pointal
! 
! laurent.pointal@limsi.fr
! 
! This software is a computer program whose purpose is to call Python code
! from Fortran programs..
! 
! This software is governed by the CeCILL-B license under French law 
! and abiding by the rules of distribution of free software.  
! You can  use, modify and/ or redistribute the software under the terms of 
! the CeCILL-B license as circulated by CEA, CNRS and INRIA at the following 
! URL "http://www.cecill.info". 
! 
! As a counterpart to the access to the source code and  rights to copy,
! modify and redistribute granted by the license, users are provided only
! with a limited warranty  and the software's author,  the holder of the
! economic rights,  and the successive licensors  have only  limited
! liability. 
! 
! In this respect, the user's attention is drawn to the risks associated
! with loading,  using,  modifying and/or developing or reproducing the
! software by the user in light of its specific status of free software,
! that may mean  that it is complicated to manipulate,  and  that  also
! therefore means  that it is reserved for developers  and  experienced
! professionals having in-depth computer knowledge. Users are therefore
! encouraged to load and test the software's suitability as regards their
! requirements in conditions enabling the security of their systems and/or 
! data to be ensured and,  more generally, to use and operate it in the 
! same conditions as regards security. 
!  
! The fact that you are presently reading this means that you have had
! knowledge of the CeCILL-B license and that you accept its terms.

MODULE forcallpy
  USE, INTRINSIC :: iso_c_binding, ONLY: c_double,c_ptr,c_loc,c_int32_t
  IMPLICIT NONE

  ! Number of items in arrays transmitted to C, must match with same C defines. 
  INTEGER,PARAMETER,PRIVATE :: SIMPLESIZE = 8
  INTEGER,PARAMETER,PRIVATE :: ARRAYSIZE = 3
 
  ! Must match with C constants.
  ENUM,BIND(c)
    ENUMERATOR :: FCP_RUNSTMT=1, FCP_SUBPROC=2
    ENUMERATOR :: FCP_INT32FCT=3, FCP_INT64FCT=4
    ENUMERATOR :: FCP_REAL32FCT=5, FCP_REAL64FCT=6
  END ENUM
  
INTERFACE
  ! =======================================================================
  ! Declaration of interface prototypes of C functions:
  !
  SUBROUTINE forcallpy_init_internal(argv0, regsig, printerr, verbosity, coderr) &
  BIND(C, NAME="forcallpy_init_internal")
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t, c_char
    IMPLICIT NONE
    CHARACTER(KIND=c_char),DIMENSION(*),INTENT(in) :: argv0
    INTEGER(c_int32_t),INTENT(in),VALUE :: regsig
    INTEGER(c_int32_t),INTENT(in),VALUE :: printerr
    INTEGER(c_int32_t),INTENT(in),VALUE :: verbosity
    INTEGER(c_int32_t),INTENT(out) :: coderr
  END SUBROUTINE forcallpy_init_internal

  SUBROUTINE forcallpy_terminate_internal(coderr) &
  BIND(C, NAME="forcallpy_terminate_internal")
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t
    IMPLICIT NONE
    INTEGER(c_int32_t),INTENT(out) :: coderr
  END SUBROUTINE forcallpy_terminate_internal
  
  SUBROUTINE forcallpy_run_internal(expr, coderr) &
  BIND(C, NAME="forcallpy_run_internal")
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t, c_char
    IMPLICIT NONE
    CHARACTER(KIND=c_char),DIMENSION(*),INTENT(in) :: expr
    INTEGER(c_int32_t),INTENT(out) :: coderr
  END SUBROUTINE forcallpy_run_internal
  
  SUBROUTINE forcallpy_expr_int32dbl_internal(sexpr,exprkind,resptr,&
  itab,ipres,ilen,ftab,fpres,flen,ptab,plen,&
  iatab,iatab_itemlen,ialen,fatab,fatab_itemlen,falen,&
  iwtab,iwtab_itemlen,iwlen,fwtab,fwtab_itemlen,fwlen,coderr) &
  BIND(C, NAME="forcallpy_expr_int32dbl_internal")
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t, c_char, c_double, c_ptr, c_int64_t
    IMPLICIT NONE
    CHARACTER(KIND=c_char),DIMENSION(*),INTENT(in) :: sexpr
    TYPE(c_ptr),INTENT(in),VALUE :: resptr
    INTEGER(c_int32_t),INTENT(in),VALUE :: exprkind,ilen,flen,plen,ialen,falen,iwlen,fwlen
    INTEGER(c_int32_t),DIMENSION(*),INTENT(in) :: itab,ipres,fpres,iatab_itemlen,&
                                        fatab_itemlen,iwtab_itemlen,fwtab_itemlen
    REAL(c_double),DIMENSION(*),INTENT(in) :: ftab
    TYPE(c_ptr),DIMENSION(*),INTENT(in) :: iatab,fatab
    TYPE(c_ptr),DIMENSION(*),INTENT(inout) :: iwtab,fwtab,ptab
    INTEGER(c_int32_t),INTENT(out) :: coderr
  END SUBROUTINE forcallpy_expr_int32dbl_internal
  
  FUNCTION forcallpy_allocate_array(fvtype, ftabpointer, dim0len, dim1len, dim2len, coderr) &
  BIND(C, NAME="forcallpy_allocate_array")
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t, c_ptr
    IMPLICIT NONE
    INTEGER(c_int32_t),INTENT(in),VALUE :: fvtype
    TYPE(c_ptr) :: ftabpointer
    INTEGER(c_int32_t),INTENT(in),VALUE :: dim0len
    INTEGER(c_int32_t),INTENT(in),VALUE :: dim1len
    INTEGER(c_int32_t),INTENT(in),VALUE :: dim2len
    INTEGER(c_int32_t),INTENT(out) :: coderr
    TYPE(c_ptr) :: forcallpy_allocate_array
  END FUNCTION forcallpy_allocate_array

END INTERFACE      

CONTAINS


  !========================================================================
  SUBROUTINE pyinit(register_sighandlers, print_errors, verbosity, coderr)
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t, c_loc, c_char, c_null_char
    IMPLICIT NONE
    ! --- Parameters and return type
    INTEGER,INTENT(in),OPTIONAL :: register_sighandlers
    INTEGER,INTENT(in),OPTIONAL :: print_errors
    INTEGER,INTENT(in),OPTIONAL :: verbosity
    INTEGER(c_int32_t),OPTIONAL,INTENT(out) :: coderr
    ! --- Locals
    CHARACTER(len=:), ALLOCATABLE :: argv0
    INTEGER :: argv0length
    INTEGER(c_int32_t),TARGET :: callerr
    INTEGER(c_int32_t) :: regsig, printerr, verbos
    
    ! Call intrinsinc function to retrieve first command line argument
    ! (normally program name + its path if providen).
    CALL get_command(length=argv0length)                ! Retrieve string length for command
    allocate(character(argv0length+1) :: argv0)         ! Dynamically allocate it (+1 byte)
    CALL get_command(argv0)                             ! Fill it with command

    ! Manage default values of optional parameters
    IF (PRESENT(register_sighandlers)) THEN
      regsig = register_sighandlers
    ELSE
      regsig = 1
    END IF
    IF (PRESENT(print_errors)) THEN
      printerr = print_errors
    ELSE
      printerr = 1
    END IF
    IF (PRESENT(verbosity)) THEN
      verbos = verbosity
    ELSE
      verbos = -1
    END IF
    
    ! Call Python stuff initalization within C code.
    argv0 = TRIM(argv0) // c_null_char                  ! Provide C string termination (0 byte)
    CALL forcallpy_init_internal(argv0, %VAL(regsig), %VAL(printerr), %VAL(verbos), %REF(callerr))
    deallocate(argv0)                                   ! Free dynamically allocated string
    
    IF (present(coderr)) THEN
        coderr = callerr
    ENDIF    
  END SUBROUTINE pyinit
  
  !========================================================================
  SUBROUTINE pyterm(coderr)
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t
    IMPLICIT NONE
    ! --- Parameters and return type
    INTEGER(c_int32_t),OPTIONAL,INTENT(out) :: coderr
    ! --- Locals
    INTEGER(c_int32_t),TARGET :: callerr
    
    CALL forcallpy_terminate_internal(%REF(callerr))
    
    IF (present(coderr)) THEN
        coderr = callerr
    ENDIF    
  END SUBROUTINE pyterm

  !========================================================================
  SUBROUTINE pyrun_i4r8(sexpr,&
    a,b,c,d,e,f,g,h,s,t,u,v,w,x,y,z,p,av,bv,cv,xv,yv,zv,aw,bw,cw,xw,yw,zw,coderr)
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t, c_int64_t, c_char, c_double, c_ptr, c_null_char, c_null_ptr, c_loc
    IMPLICIT NONE
    ! --- Parameters and return type
    CHARACTER*(*),INTENT(in) :: sexpr
    INTEGER(c_int32_t),INTENT(in),OPTIONAL :: a,b,c,d,e,f,g,h
    REAL(c_double),INTENT(in),OPTIONAL :: s,t,u,v,w,x,y,z
    TYPE(c_ptr),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: p
    INTEGER(c_int32_t),INTENT(in),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: av,bv,cv
    INTEGER(c_int32_t),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: aw,bw,cw
    REAL(c_double),INTENT(in),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: xv,yv,zv
    REAL(c_double),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: xw,yw,zw
    INTEGER(c_int32_t),OPTIONAL,INTENT(out) :: coderr
    ! --- Locals
    INTEGER(c_int64_t),TARGET :: res
    ! Simply ignore result.
    CALL i4r8_wrap(sexpr,FCP_RUNSTMT,c_loc(res),&
    a,b,c,d,e,f,g,h,s,t,u,v,w,x,y,z,p,av,bv,cv,xv,yv,zv,aw,bw,cw,xw,yw,zw,coderr)
  END SUBROUTINE pyrun_i4r8
 
  !========================================================================
  SUBROUTINE pysub_i4r8(sexpr,&
    a,b,c,d,e,f,g,h,s,t,u,v,w,x,y,z,p,av,bv,cv,xv,yv,zv,aw,bw,cw,xw,yw,zw,coderr)
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t, c_int64_t, c_char, c_double, c_ptr, c_null_char, c_null_ptr, c_loc
    IMPLICIT NONE
    ! --- Parameters and return type
    CHARACTER*(*),INTENT(in) :: sexpr
    INTEGER(c_int32_t),INTENT(in),OPTIONAL :: a,b,c,d,e,f,g,h
    REAL(c_double),INTENT(in),OPTIONAL :: s,t,u,v,w,x,y,z
    TYPE(c_ptr),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: p
    INTEGER(c_int32_t),INTENT(in),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: av,bv,cv
    INTEGER(c_int32_t),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: aw,bw,cw
    REAL(c_double),INTENT(in),OPTIONAL,DIMENSION(:),TARGET :: xv,yv,zv
    REAL(c_double),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: xw,yw,zw
    INTEGER(c_int32_t),OPTIONAL,INTENT(out) :: coderr    
    ! --- Locals
    INTEGER(c_int64_t),TARGET :: res
    ! Simply ignore result.
    CALL i4r8_wrap(sexpr,FCP_SUBPROC,c_loc(res),&
    a,b,c,d,e,f,g,h,s,t,u,v,w,x,y,z,p,av,bv,cv,xv,yv,zv,aw,bw,cw,xw,yw,zw,coderr)
  END SUBROUTINE pysub_i4r8
  
  !========================================================================
  FUNCTION pyfct_i4r8_i4(sexpr,&
    a,b,c,d,e,f,g,h,s,t,u,v,w,x,y,z,p,av,bv,cv,xv,yv,zv,aw,bw,cw,xw,yw,zw,coderr)
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t, c_int64_t, c_char, c_double, c_ptr, c_loc
    IMPLICIT NONE
    ! --- Parameters and return type
    INTEGER*4 :: pyfct_i4r8_i4
    CHARACTER*(*),INTENT(in) :: sexpr
    INTEGER(c_int32_t),INTENT(in),OPTIONAL :: a,b,c,d,e,f,g,h
    REAL(c_double),INTENT(in),OPTIONAL :: s,t,u,v,w,x,y,z
    TYPE(c_ptr),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: p
    INTEGER(c_int32_t),INTENT(in),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: av,bv,cv
    INTEGER(c_int32_t),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: aw,bw,cw
    REAL(c_double),INTENT(in),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: xv,yv,zv
    REAL(c_double),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: xw,yw,zw
    INTEGER(c_int32_t),OPTIONAL,INTENT(out) :: coderr
    ! --- Locals
    INTEGER(c_int32_t),TARGET :: res
    CALL i4r8_wrap(sexpr,FCP_INT32FCT,c_loc(res),&
    a,b,c,d,e,f,g,h,s,t,u,v,w,x,y,z,p,av,bv,cv,xv,yv,zv,aw,bw,cw,xw,yw,zw,coderr)
    pyfct_i4r8_i4 = res
  END FUNCTION pyfct_i4r8_i4

  !========================================================================
  FUNCTION pyfct_i4r8_r8(sexpr,&
    a,b,c,d,e,f,g,h,s,t,u,v,w,x,y,z,p,av,bv,cv,xv,yv,zv,aw,bw,cw,xw,yw,zw,coderr)
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t, c_int64_t, c_char, c_double, c_ptr, c_loc
    IMPLICIT NONE
    ! --- Parameters and return type
    REAL(8) :: pyfct_i4r8_r8
    CHARACTER*(*),INTENT(in) :: sexpr
    INTEGER(c_int32_t),INTENT(in),OPTIONAL :: a,b,c,d,e,f,g,h
    REAL(c_double),INTENT(in),OPTIONAL :: s,t,u,v,w,x,y,z
    TYPE(c_ptr),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: p
    INTEGER(c_int32_t),INTENT(in),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: av,bv,cv
    INTEGER(c_int32_t),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: aw,bw,cw
    REAL(c_double),INTENT(in),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: xv,yv,zv
    REAL(c_double),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: xw,yw,zw
    INTEGER(c_int32_t),OPTIONAL,INTENT(out) :: coderr
    ! --- Locals
    REAL(c_double),TARGET :: res
    CALL i4r8_wrap(sexpr,FCP_REAL64FCT,c_loc(res),&
    a,b,c,d,e,f,g,h,s,t,u,v,w,x,y,z,p,av,bv,cv,xv,yv,zv,aw,bw,cw,xw,yw,zw,coderr)
    pyfct_i4r8_r8 = res
  END FUNCTION pyfct_i4r8_r8

  !========================================================================
  FUNCTION pyfct_i4r8_r4(sexpr,&
    a,b,c,d,e,f,g,h,s,t,u,v,w,x,y,z,p,av,bv,cv,xv,yv,zv,aw,bw,cw,xw,yw,zw,coderr)
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t, c_int64_t, c_char, c_float, c_double, c_ptr, c_loc
    IMPLICIT NONE
    ! --- Parameters and return type
    REAL(4) :: pyfct_i4r8_r4
    CHARACTER*(*),INTENT(in) :: sexpr
    INTEGER(c_int32_t),INTENT(in),OPTIONAL :: a,b,c,d,e,f,g,h
    REAL(c_double),INTENT(in),OPTIONAL :: s,t,u,v,w,x,y,z
    TYPE(c_ptr),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: p
    INTEGER(c_int32_t),INTENT(in),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: av,bv,cv
    INTEGER(c_int32_t),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: aw,bw,cw
    REAL(c_double),INTENT(in),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: xv,yv,zv
    REAL(c_double),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: xw,yw,zw
    INTEGER(c_int32_t),OPTIONAL,INTENT(out) :: coderr
    ! --- Locals
    REAL(c_float),TARGET :: res
    CALL i4r8_wrap(sexpr,FCP_REAL32FCT,c_loc(res),&
    a,b,c,d,e,f,g,h,s,t,u,v,w,x,y,z,p,av,bv,cv,xv,yv,zv,aw,bw,cw,xw,yw,zw,coderr)
    pyfct_i4r8_r4 = res
  END FUNCTION pyfct_i4r8_r4
  
  !========================================================================
  FUNCTION pyfct_i4r8_bool(sexpr,&
    a,b,c,d,e,f,g,h,s,t,u,v,w,x,y,z,p,av,bv,cv,xv,yv,zv,aw,bw,cw,xw,yw,zw,coderr)
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t, c_int64_t, c_char, c_double, c_ptr, c_loc
    IMPLICIT NONE
    ! --- Parameters and return type
    LOGICAL :: pyfct_i4r8_bool
    CHARACTER*(*),INTENT(in) :: sexpr
    INTEGER(c_int32_t),INTENT(in),OPTIONAL :: a,b,c,d,e,f,g,h
    REAL(c_double),INTENT(in),OPTIONAL :: s,t,u,v,w,x,y,z
    TYPE(c_ptr),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: p
    INTEGER(c_int32_t),INTENT(in),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: av,bv,cv
    INTEGER(c_int32_t),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: aw,bw,cw
    REAL(c_double),INTENT(in),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: xv,yv,zv
    REAL(c_double),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: xw,yw,zw
    INTEGER(c_int32_t),OPTIONAL,INTENT(out) :: coderr
    ! --- Locals
    INTEGER(c_int64_t),TARGET :: res
    CALL i4r8_wrap(sexpr,FCP_INT64FCT,c_loc(res),&
    a,b,c,d,e,f,g,h,s,t,u,v,w,x,y,z,p,av,bv,cv,xv,yv,zv,aw,bw,cw,xw,yw,zw,coderr)
    pyfct_i4r8_bool = (res .NE. 0)
  END FUNCTION pyfct_i4r8_bool


  !========================================================================
  SUBROUTINE i4r8_wrap(sexpr,exprkind,resptr,&
    a,b,c,d,e,f,g,h,s,t,u,v,w,x,y,z,p,av,bv,cv,xv,yv,zv,aw,bw,cw,xw,yw,zw,coderr)
    USE, INTRINSIC :: iso_c_binding, ONLY: c_int32_t, c_int64_t, c_char, c_double, c_ptr, c_null_char, c_null_ptr, c_loc
    IMPLICIT NONE
    ! --- Parameters and return type
    CHARACTER*(*),INTENT(in) :: sexpr
    INTEGER(c_int32_t),INTENT(in),VALUE :: exprkind
    TYPE(c_ptr),INTENT(in),VALUE :: resptr
    INTEGER(c_int32_t),INTENT(in),OPTIONAL :: a,b,c,d,e,f,g,h
    REAL(c_double),INTENT(in),OPTIONAL :: s,t,u,v,w,x,y,z
    TYPE(c_ptr),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: p
    INTEGER(c_int32_t),INTENT(in),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: av,bv,cv
    INTEGER(c_int32_t),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: aw,bw,cw
    REAL(c_double),INTENT(in),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: xv,yv,zv
    REAL(c_double),INTENT(inout),OPTIONAL,DIMENSION(:),CONTIGUOUS,TARGET :: xw,yw,zw
    INTEGER(c_int32_t),OPTIONAL,INTENT(out) :: coderr
    ! --- Locals
    CHARACTER(len=:), ALLOCATABLE :: cexpr
    INTEGER :: exprlength, i  
    INTEGER(c_int32_t),DIMENSION(SIMPLESIZE),TARGET :: itab
    INTEGER(c_int32_t),DIMENSION(SIMPLESIZE),TARGET :: ipres
    REAL(c_double),DIMENSION(SIMPLESIZE),TARGET :: ftab
    INTEGER(c_int32_t),DIMENSION(SIMPLESIZE),TARGET :: fpres
    INTEGER(c_int32_t) :: ilen,flen,ppres,plen,ialen,falen,iwlen,fwlen
    TYPE(c_ptr),DIMENSION(ARRAYSIZE),TARGET :: iatab,fatab,iwtab,fwtab
    INTEGER(c_int32_t),DIMENSION(ARRAYSIZE),TARGET :: iatab_itemlen,fatab_itemlen,iwtab_itemlen,fwtab_itemlen
    INTEGER(c_int32_t),TARGET :: callerr
    TYPE(c_ptr) :: ptab
    
    exprlength = LEN_TRIM(sexpr)                        ! Get necessary length of string
    allocate(character(exprlength+1) :: cexpr)          ! Dynamically allocate expression (+1 byte)
    cexpr = TRIM(sexpr) // c_null_char                  ! Provide C string termination (0 byte)

    ! Initialize arrays with default values.
    DO i=1,SIMPLESIZE
        itab(i) = 0
        ipres(i) = 0
        ftab(i) = 0.D0
        fpres(i) = 0
    ENDDO
    DO i=1,ARRAYSIZE
        iatab(i) = c_null_ptr
        fatab(i) = c_null_ptr
        iwtab(i) = c_null_ptr
        fwtab(i) = c_null_ptr
    ENDDO
    
    ! Copy available parameters inside arrays.
    ! Integer values ------------------------------------------------------
    IF (present(a)) THEN
        itab(1) = a
        ipres(1) = 1
    ELSE
        ipres(1) = 0
    ENDIF
    IF (present(b)) THEN
        itab(2) = b
        ipres(2) = 1
    ELSE
        ipres(2) = 0
    ENDIF
    IF (present(c)) THEN
        itab(3) = c
        ipres(3) = 1
    ELSE
        ipres(3) = 0
    ENDIF
    IF (present(d)) THEN
        itab(4) = d
        ipres(4) = 1
    ELSE
        ipres(4) = 0
    ENDIF
    IF (present(e)) THEN
        itab(5) = e
        ipres(5) = 1
    ELSE
        ipres(5) = 0
    ENDIF
    IF (present(f)) THEN
        itab(6) = f
        ipres(6) = 1
    ELSE
        ipres(6) = 0
    ENDIF
    IF (present(g)) THEN
        itab(7) = g
        ipres(7) = 1
    ELSE
        ipres(7) = 0
    ENDIF
    
    IF (present(h)) THEN
        itab(8) = h
        ipres(8) = 1
    ELSE
        ipres(8) = 0
    ENDIF
    ! Double values -------------------------------------------------------
    IF (present(s)) THEN
        ftab(8) = s
        fpres(8) = 1
    ELSE
        fpres(8) = 0
    ENDIF
    IF (present(t)) THEN
        ftab(7) = t
        fpres(7) = 1
    ELSE
        fpres(7) = 0
    ENDIF
    IF (present(u)) THEN
        ftab(6) = u
        fpres(6) = 1
    ELSE
        fpres(6) = 0
    ENDIF
    IF (present(v)) THEN
        ftab(5) = v
        fpres(5) = 1
    ELSE
        fpres(5) = 0
    ENDIF
    IF (present(w)) THEN
        ftab(4) = w
        fpres(4) = 1
    ELSE
        fpres(4) = 0
    ENDIF
    IF (present(x)) THEN
        ftab(3) = x
        fpres(3) = 1
    ELSE
        fpres(3) = 0
    ENDIF
    IF (present(y)) THEN
        ftab(2) = y
        fpres(2) = 1
    ELSE
        fpres(2) = 0
    ENDIF
    IF (present(z)) THEN
        ftab(1) = z
        fpres(1) = 1
    ELSE
        fpres(1) = 0
    ENDIF
    ! Arrays of rw pointers -----------------------------------------------
    ptab = c_null_ptr
    plen = 0
    IF (present(p)) THEN
        IF (SIZE(p) > 0) THEN
            ptab = c_loc(p(1))  ! Direct address of array items
            plen = SIZE(p)
        ENDIF
    ENDIF
    ! Integer arrays readonly ---------------------------------------------
    IF (present(av)) THEN
        IF (SIZE(av) > 0) THEN
            iatab(1) = c_loc(av(1))
            iatab_itemlen(1) = SIZE(av)
        ENDIF
    ENDIF
    IF (present(bv)) THEN
        IF (SIZE(bv) > 0) THEN
            iatab(2) = c_loc(bv(1))
            iatab_itemlen(2) = SIZE(bv)
        ENDIF
    ENDIF
    IF (present(cv)) THEN
        IF (SIZE(cv) > 1) THEN
            iatab(3) = c_loc(cv(1))
            iatab_itemlen(3) = SIZE(cv)
        ENDIF
    ENDIF   
    ! Double arrays readonly ----------------------------------------------
    IF (present(zv)) THEN
        IF (SIZE(zv) > 0) THEN
            fatab(1) = c_loc(zv(1))
            fatab_itemlen(1) = SIZE(zv)
        ENDIF
    ENDIF
    IF (present(yv)) THEN
        IF (SIZE(yv) > 0) THEN
            fatab(2) = c_loc(yv(1))
            fatab_itemlen(2) = SIZE(yv)
        ENDIF
    ENDIF
    IF (present(xv)) THEN
        IF (SIZE(xv) > 0) THEN
            fatab(3) = c_loc(xv(1))
            fatab_itemlen(3) = SIZE(xv)
        ENDIF
    ENDIF    
    ! Integer arrays readwrite --------------------------------------------
    IF (present(aw)) THEN
        IF (SIZE(aw) > 0) THEN
            iwtab(1) = c_loc(aw(1))
            iwtab_itemlen(1) = SIZE(aw)
        ENDIF
    ENDIF
    IF (present(bw)) THEN
        IF (SIZE(bw) >  0) THEN
            iwtab(2) = c_loc(bw(1))
            iwtab_itemlen(2) = SIZE(bw)
        ENDIF
    ENDIF
    IF (present(cw)) THEN
        IF (SIZE(cw) > 0) THEN
            iwtab(3) = c_loc(cw(1))
            iwtab_itemlen(3) = SIZE(cw)
        ENDIF
    ENDIF
    ! Double arrays readwrite ---------------------------------------------
    IF (present(zw)) THEN
        IF (SIZE(zw) > 0) THEN
            fwtab(1) = c_loc(zw(1))
            fwtab_itemlen(1) = SIZE(zw)
        ENDIF
    ENDIF
    IF (present(yw)) THEN
        IF (SIZE(yw) > 0) THEN
            fwtab(2) = c_loc(yw(1))
            fwtab_itemlen(2) = SIZE(yw)
        ENDIF
    ENDIF
    IF (present(xw)) THEN
        IF (SIZE(xw) > 0) THEN
            fwtab(3) = c_loc(xw(1))
            fwtab_itemlen(3) = SIZE(xw)
        ENDIF
    ENDIF
    
    ilen = SIZE(itab)
    flen = SIZE(ftab)
    ialen = SIZE(iatab)
    falen = SIZE(fatab)
    iwlen = SIZE(iwtab)
    fwlen = SIZE(fwtab)

    CALL forcallpy_expr_int32dbl_internal(%REF(cexpr),%VAL(exprkind),%REF(resptr),&
        %REF(itab),%REF(ipres),%VAL(ilen),%REF(ftab),%REF(fpres),%VAL(flen),%REF(ptab),%VAL(plen),&
        %REF(iatab),%REF(iatab_itemlen),%VAL(ialen),%REF(fatab),%REF(fatab_itemlen),%VAL(falen),&
        %REF(iwtab),%REF(iwtab_itemlen),%VAL(iwlen),%REF(fwtab),%REF(fwtab_itemlen),%VAL(fwlen),%REF(callerr))

    deallocate(cexpr)                                   ! Free dynamically allocated string    
        
    IF (present(coderr)) THEN
        coderr = callerr
    ENDIF
  END SUBROUTINE i4r8_wrap
  
 
  
!   SUBROUTINE forcallpy_falloc_i4(itab, ilen)
!     INTEGER(c_int32_t),INTENT(in),VALUE :: exprkind
!     INTEGER,INTENT(in),VALUE :: ilen
! 
!   END SUBROUTINE forcallpy_falloc_i4

  SUBROUTINE forcallpy_allocate_r8(ftab, flen) BIND(C)
    USE, INTRINSIC :: iso_c_binding, ONLY: c_double,c_ptr,c_loc,c_int32_t
    IMPLICIT NONE
    ! --- Parameters and return type
    REAL(c_double),INTENT(out),DIMENSION(:) :: ftab
    INTEGER(c_int32_t),INTENT(in),VALUE :: flen
    ! --- Locals
    TYPE(REAL(8)),POINTER,DIMENSION(:),SAVE :: data
    allocate(data(flen))
    data(1) = 12.3d00
    data(2) = -12.3d00
    ftab = data
  END SUBROUTINE forcallpy_allocate_r8
  
!   SUBROUTINE forcallpy_deallocate_r8(ftab, flen) BIND(C)
!     USE, INTRINSIC :: iso_c_binding, ONLY: c_double,c_ptr,c_null_ptr
!     ! --- Parameters and return type
!     TYPE(c_ptr),INTENT(inout) :: ftab
!     INTEGER,INTENT(in),VALUE :: flen    
!     deallocate(data)
!     data = c_null_ptr
!   END SUBROUTINE
! Interfaces with C code which dont need wrapper.

! See 
!   http://fortranwiki.org/fortran/show/ISO_C_BINDING
!   https://gcc.gnu.org/onlinedocs/gfortran/Interoperability-with-C.html
!   https://gcc.gnu.org/onlinedocs/gfortran/Interoperable-Subroutines-and-Functions.html

END MODULE forcallpy


