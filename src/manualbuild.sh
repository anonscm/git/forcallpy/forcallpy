# This is just an example of manual build of the library, in
# case you want to use it out of CMake meta-build system.

# Compilation and link options for Python
COMPIL_FLAGS=$(python3-config --cflags)
#$ python3-config --cflags
#-I/usr/include/python3.5m -I/usr/include/python3.5m  -Wno-unused-result -Wsign-compare -g -fstack-protector-strong -Wformat -Werror=format-security  -DNDEBUG -g -fwrapv -O3 -Wall -Wstrict-prototypes

LINK_FLAGS=$(python3-config --ldflags)
#$ python3-config --ldflags
#-L/usr/lib/python3.5/config-3.5m-x86_64-linux-gnu -L/usr/lib -lpython3.5m -lpthread -ldl  -lutil -lm  -Xlinker -export-dynamic -Wl,-O1 -Wl,-Bsymbolic-functions

# ----- Build the library:
gcc ${COMPIL_FLAGS} -c forcallpy_cwrapper.c -o forcallpy_cwrapper.o
gfortran -c forcallpy_interface.f90 -o forcallpy_interface.o
ar -rcs libforcallpy.a forcallpy_cwrapper.o forcallpy_interface.o

# ----- And if you have some fortran program to link with this library:
#gfortran -c myprogram.f03 -o myprogram.o
#gfortran -o myprogram myprogram.o -L . -l forcallpy ${LINK_FLAGS}
#   (adapt -L . to installation path of library)


