/*
Project: forcallpy
License: CeCILL-B (BSD like)
    
Copyright CNRS/LIMSI (2017) by Laurent Pointal

laurent.pointal@limsi.fr

This software is a computer program whose purpose is to call Python code
from Fortran programs.

This software is governed by the CeCILL-B license under French law and 
abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms of 
the CeCILL-B license as circulated by CEA, CNRS and 
INRIA at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

#pragma once
#ifndef INCLUDED_FORTRANTOPYTHON__H
#define INCLUDED_FORTRANTOPYTHON__H


#include <stdint.h>     // To have well know int sizes.

void 
forcallpy_init_internal(
        const char* argv0, 
        int32_t register_sighandlers,
        int32_t print_errors,
        int32_t verbosity,
        int32_t* coderr);

void 
forcallpy_terminate_internal(
        int32_t* coderr);

void 
forcallpy_expr_int32dbl_internal(
        const char* sexpr,
        int32_t exprkind,
        void* resptr,
        int32_t* itab,
        int32_t* ipres,
        int32_t ilen,
        double* ftab,
        int32_t* fpres,
        int32_t flen,
        void** ptab,
        int32_t plen,
        void** iatab,
        int32_t* iatab_itemlen,
        int ialen,
        void** fatab,
        int32_t* fatab_itemlen,
        int falen,   
        void** iwtab,
        int32_t* iwtab_itemlen,
        int iwlen,
        void** fwtab,
        int32_t* fwtab_itemlen,
        int fwlen,         
        int32_t* coderr);

// Following functions are defined in Fortran interface.
// (to be called by C)
void 
forcallpy_allocate_r4(
        float** ftab, 
        int32_t flen);

void 
forcallpy_allocate_r8(
        double** ftab, 
        int32_t flen);

void 
forcallpy_allocate_i4(
        int32_t** itab, 
        int32_t flen);

void 
forcallpy_allocate_i8(
        int64_t** itab, 
        int32_t flen);

#endif  // INCLUDED_FORTRANTOPYTHON__H

