/*
Project: forcallpy
License: CeCILL-B (BSD like)
    
Copyright CNRS/LIMSI (2017) by Laurent Pointal

laurent.pointal@limsi.fr

This software is a computer program whose purpose is to call Python 
from Fortran programs.

This software is governed by the CeCILL-B license under French law and 
abiding by the rules of distribution of free software.  
You can  use, modify and/ or redistribute the software under the terms of 
the CeCILL-B license as circulated by CEA, CNRS and 
INRIA at the following URL "http://www.cecill.info". 

As a counterpart to the access to the source code and  rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty  and the software's author,  the holder of the
economic rights,  and the successive licensors  have only  limited
liability. 

In this respect, the user's attention is drawn to the risks associated
with loading,  using,  modifying and/or developing or reproducing the
software by the user in light of its specific status of free software,
that may mean  that it is complicated to manipulate,  and  that  also
therefore means  that it is reserved for developers  and  experienced
professionals having in-depth computer knowledge. Users are therefore
encouraged to load and test the software's suitability as regards their
requirements in conditions enabling the security of their systems and/or 
data to be ensured and,  more generally, to use and operate it in the 
same conditions as regards security. 

The fact that you are presently reading this means that you have had
knowledge of the CeCILL-B license and that you accept its terms.
*/

/**
 * Wrap Python embedding into a simple C library callable by Fortran programs.
 * Out of the Python interpreter initialization and termination, the library
 * provides functions to make call to Python callables using a predefined 
 * generic set of parameters. It is up to both Fortran and Python sides to 
 * agree on these parameters usage.
 * As it targets scientific computing, arrays of data are transmitted as 
 * numpy arrays.
 *
 * A link for fortran developers: http://www.fortran90.org/src/rosetta.html
 * And a tool for use of fortran inside Jupyter / IPython:
 * https://pypi.python.org/pypi/fortran-magic
 */

// May be useful links:
// https://stackoverflow.com/questions/14843408/python-c-embedded-segmentation-fault
// https://www.awasu.com/weblog/embedding-python/threads/
// https://docs.scipy.org/doc/numpy-1.13.0/reference/internals.html
// http://scv.bu.edu/computation/bluegene/IBMdocs/compiler/xlf-10.1/html/xlfopg/interlang-passdata.htm#interlang-passpoArgs
// http://scipy-cookbook.readthedocs.io/items/C_Extensions_NumPy_arrays.html
// https://mathieu.fenniak.net/embedding-python-tips/
// http://pages.tacc.utexas.edu/~eijkhout/istc/html/language.html

// Numpy array to own allocated memory:
// https://stackoverflow.com/questions/37988849/safer-way-to-expose-a-c-allocated-memory-buffer-using-numpy-ctypes
// Voir refs https://docs.scipy.org/doc/numpy-1.13.0/genindex.html
// http://numpy-discussion.10968.n7.nabble.com/Initializing-array-from-buffer-td39137.html
// ==> https://docs.scipy.org/doc/numpy/reference/c-api.array.html#c.PyArray_New
// https://gist.github.com/jdfr/688507524b6b4163e4c0

// C/Fortran memory allocation…
// https://stackoverflow.com/questions/23891769/allocating-memory-in-c-for-a-fortran-allocatable
// https://gcc.gnu.org/onlinedocs/gfortran/Further-Interoperability-of-Fortran-with-C.html
// https://stackoverflow.com/questions/16385372/allocating-memory-in-c-for-a-fortran-array

//TODO: see if can provide via numpy an indexing scheme of arrays similar to fortran one
// (ie. N to M index in place of 0 to (M-N)).

//TODO: Setup tests for memory leaks, for parameters use…

// C <=> Python interface.
// https://docs.python.org/3/extending/embedding.html
// Limitation of interface to stable ABI: https://www.python.org/dev/peps/pep-0384/
//#define Py_LIMITED_API
#include <Python.h>

/** Policy used about PyObject and reference counting.
 * 
 * All PyObject* pointers are initialized to NULL.
 * Pointers containing borrowed references are ignored.
 * Pointers with new references are Py_CLEARed, if their reference is
 * stolen, then they are immediatly set to NULL.
 */

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
// Note: this defines the _import_array as a static function here.
#include <numpy/arrayobject.h>

// Prototypes (mines).
#include "forcallpy_cwrapper.h"
// Generated file from version numbering.
#include "forcallpy_version.h"

// Standard includes.
#include <stdio.h>
#include <stdint.h>
#include <assert.h>
#include <float.h>


// ===== Preprocessor definitions =========================================
// This definition allow to see enter/exit to C Python API, useful when there
// is some hard crash (this is plain old "printf" debugging, may also use a debugger,
// supporting Fortran + C + Python…)
//#define TRACE_PYTHON_CALLS      1

#ifdef TRACE_PYTHON_CALLS
#define TRACE_IN(x)        fprintf(stderr,"    ==> " x "\n")
#define TRACE_OUT(x)       fprintf(stderr,"    <== " x "\n")
#else
#define TRACE_IN(x)        NULL
#define TRACE_OUT(x)       NULL
#endif

// Prefix strings for traces via fprintf.
#define ENTERCALL          " ==> "
#define EXITCALL           " <== "
#define PARAM              "       +param "
#define INTERNVALUE        "           $value "

// ===== Enums and Constants ==============================================
/**
 * \brief Error codes for the library.
 */
enum {
    ERROR_NOT_SET = -1,         //<! Miss to set error code before return.
    NOERROR = 0,                //<! All right.
    PYTHON_NOT_READY=1,         //<! Python has not been initialized/has been finized.
    FAILED_DECODE_ARG0 = 3,     //<! Failed decoding of forcallpy_init() parameter.
    FAILED_PYTHON_CODE = 4,     //<! Failure during Python code execution.
    FAILED_INIT_FCPMODULE = 5   //<! Failure to initialize forcallpy module table.
    };

/**
 * \brief Status of Python interpreter initialization.
 * 
 * We prohibit cycles of initialization/finalization of the interpreter as
 * it may generate problems with loaded modules as Numpy. 
 * Multiples initializations can occur (this allows to change some global trace or error
 * processing), multiple finalizations can occur, but once finalized Python interpreter 
 * cannot be re-initialized.
 */
enum {
    UNINITIALIZED = 0,
    INITIALIZED,
    FINALIAZED
    };

/**
 * \brief Level of trace for debugging.
 * 
 * Higher is the verbose level, more verbose is the output.
 */
enum {
    VERB_NONE = 0,                          //!< No extra trace.
    VERB_CALLS,                             //!< Trace library functions calls and main parameters.
    VERB_CALLSOUT,                          //!< Trace exit from functions.
    VERB_PYCALLS,                           //!< Trace call to Python functions.
    VERB_PARAMS_SAMPLE,                     //!< Trace sample of functions parameters and return values.
    VERB_PARAMS_ALL,                        //!< Trace all (detailed functions parameters).
    VERB_INTERNAL                           //!< Trace internal built values.
    };

/**
 * \brief Kind of expression given by caller (and attended result).
 * 
 * Fortran has same constants definitions.
 */
enum {
    EXPKIND_RUNSTMT=1,                      //!< Statements to run as is (maybe multiline).
    EXPKIND_SUBPROC=2,                      //!< Subroutine procedure (ie. ignore result).
    EXPKIND_INT32FCT=3,                     //!< Function returning an integer, target 32 bits.
    EXPKIND_INT64FCT=4,                     //!< Function returning an integer, target 64 bits.
    EXPKIND_REAL32FCT=5,                    //!< Function returning a real, target 32 bits float. 
    EXPKIND_REAL64FCT=6                     //!< Function returning a real, target 64 bits double.
    };

// For readonly flag in functions.
#define READONLY        1
#define READWRITE       0    
    
// Maximum count of items to allow before switching to sample view.
#define SAMPLE_MAXITESMS    10
// Count of sample values to dump at begin and end with VERB_PARAMS_SAMPLE verbosity level.
#define SAMPLE_BEGEND       3

// Number of items in arrays transmitted from Fortran. Must match with same Fortran parameters.
#define SIMPLESIZE          8
#define ARRAYSIZE           3


// ===== Types ============================================================
/**
 * \brief Types of data for array allocations.
 * 
 * Fortran has same constants definitions.
 */enum {
    FCP_INT32 = 1,
    FCP_INT64,
    FCP_REAL32,
    FCP_REAL64
    };
    

// ===== Globals ==========================================================
// We use g_ prefix for globals.
static int g_pythonstate = UNINITIALIZED;   //!< Lib initialization status.
static wchar_t *g_program = NULL;           //!< Program name.
static PyThreadState *g_mainstate = NULL;   //!< Python main thread state.
static int g_print_errors = 1;              //!< Default print tracebacks of exceptions.
static int g_verbosity = VERB_NONE;         //!< What to print (debugging…).
// Note: for Python objects (PyObject* pointer), we use po prefix.
PyObject* g_poGlobals = NULL;               //!< Our global namespace for execution.


// ===== Fortran prototypes ===============================================
void* 
c_forcallpy_ptrallocate1d_r8(
    void* tab, 
    int32_t dim0, 
    int32_t* coderr);
void* 
c_forcallpy_allocate1d_r8(
    void* tab, 
    int32_t dim0, 
    int32_t* coderr);

void
c_forcallpy_deallocate1d_r8(
    void* tab, 
    int32_t* coderr);


// ===== Local prototypes =================================================
static int 
build_simple_values(
        int intsize,
        void* itab,
        int32_t* ipres,
        int ilen,
        int realsize,
        void* ftab,
        int32_t* fpres,
        int flen,
        PyObject* poLocals);

static int 
build_array_values(
        int intsize,
        void** itab,
        int32_t* itab_itemlen,
        int ilen,
        int realsize,
        void** ftab,
        int32_t* ftab_itemlen,
        int flen,
        int readonly,
        PyObject* poNamespace);

static int 
build_pointers_value(
        void** ptab,
        int plen,
        int readonly,
        const char* varname,
        PyObject* poNamespace);

static int
my_import_array(void);

static PyObject*
pyinit_forcallpy(void);

PyObject* 
forcallpy_allocate_array(
        void* ftabpointer, 
        int32_t bpointer,
        int32_t fcptype,
        int32_t dim0len,
        int32_t dim1len,
        int32_t dim2len);

void
forcallpy_deallocate_array(
        void* ftabpointer,
        int32_t bpointer,
        int32_t fcptype,
        int32_t ndims);


static PyObject*
pyfct_forcallpy_allocate(PyObject *self, PyObject *args);

static PyObject*
pyfct_forcallpy_deallocate(PyObject *self, PyObject *args);


// ========================================================================
/**
 * \brief Initialize internal stuff of forcallpy library.
 * 
 * \param[in] argv0 Program name (normally taken from command line).
 * \param[in] register_sighandlers Boolean to enable Python registration of signal handlers.
 * \param[in] print_errors Boolean to enable/disable defalt print of evaluation errors (exceptions).
 * \param[in] verbosity Integer in VERB_* constants to make wrapper more/less verbose.
 * \param[out] coderr Integer address to return error code.
 */
void forcallpy_init_internal(
        const char* argv0, 
        int32_t register_sighandlers,
        int32_t print_errors,
        int32_t verbosity,
        int32_t* coderr) 
{
PyObject* poBuiltins = NULL;
PyObject* poNumpy = NULL;
PyObject* poForcallpy = NULL;
int ok = 0;

if (g_verbosity >= VERB_CALLS)
    fprintf(stderr, ENTERCALL "forcallpy_init_internal: \"%s\", %d\n", argv0, (int)register_sighandlers);

// ===== Take care of general configuration for printfs and traces.
// This imply that multiple calls to this function allow to modify
// these running parameters.
g_print_errors = (print_errors != 0);
g_verbosity = verbosity;

// ===== Immediate exit if called at wrong time.
if (g_pythonstate == INITIALIZED)   // Allow init several times… do nothing more after first one.
    {
    *coderr = NOERROR;
    return;
    }
if (g_pythonstate == FINALIAZED)    // Disallow re-init after terminate (problem with loaded modules)
    {
    fprintf(stderr, "Embedded Python failure: python already finalized, cannot re-init\n");
    *coderr = PYTHON_NOT_READY;
    return;
    }

// ===== Prepare interpreter initialization and initilize it.
*coderr = ERROR_NOT_SET;

TRACE_IN("Py_DecodeLocale");
g_program = Py_DecodeLocale(argv0, NULL);
TRACE_OUT("Py_DecodeLocale");
if (g_program == NULL) 
    {
    fprintf(stderr, "Embedded Python failure: cannot decode argv0\n");
    if (coderr) *coderr = FAILED_DECODE_ARG0;
    return;
    }    

TRACE_IN("Py_SetProgramName");
Py_SetProgramName(g_program);       // Identify process.
TRACE_OUT("Py_SetProgramName");

// Important: this should be called BEFORE Py_Initialize().
TRACE_IN("PyImport_AppendInittab forcallpy");
ok = PyImport_AppendInittab("forcallpy", &pyinit_forcallpy);
TRACE_OUT("PyImport_AppendInittab");
if (ok == -1)
    {
    fprintf(stderr, "Failure in PyImport_AppendInittab() to init forcallpy\n");
    if (coderr) *coderr = FAILED_DECODE_ARG0;
    return;
    }

if (register_sighandlers)
    {
    TRACE_IN("Py_Initialize");
    Py_Initialize();                // Setup interpreter.
    TRACE_OUT("Py_Initialize");
    }
else
    {
    TRACE_IN("Py_InitializeEx");
    Py_InitializeEx(0);             // … without registering signal handlers
    TRACE_OUT("Py_InitializeEx");
    }

// Note (see Python C API doc), this dont set sys.argv neither the Python path (except
// standard locations for standard libs related to the interpreter).
// For that, must call PySys_SetArgvEx().
    
TRACE_IN("PyEval_InitThreads");
// Allow multithreading within Python, this acquire le GIL !
PyEval_InitThreads();               // +++++++++++++++++++++++++++++++ +GIL
TRACE_OUT("PyEval_InitThreads");

// ===== Setup environment for our scripts execution.

// Build our namespace with its globals.
TRACE_IN("PyEval_GetBuiltins -> poBuiltins");
poBuiltins = PyEval_GetBuiltins();  // Borrowed reference.
TRACE_OUT("PyEval_GetBuiltins");
if (poBuiltins == NULL)
    {
    fprintf(stderr, "Failure in PyEval_GetBuiltins()\n");
    goto ERROR_IN_INIT;
    }
TRACE_IN("PyDict_New -> g_poGlobals");
g_poGlobals  = PyDict_New();
TRACE_OUT("PyDict_New");
if (g_poGlobals == NULL)
    {
    fprintf(stderr, "Failure in PyDict_New() to create globals\n");
    goto ERROR_IN_INIT;
    }
TRACE_IN("PyDict_SetItemString -> poBuiltins ref in g_poGlobals");
PyDict_SetItemString(g_poGlobals, "__builtins__",  poBuiltins);
TRACE_OUT("PyDict_SetItemString");

// Make Numpy C API available.
// Fill'in function-pointer table and points correct variables to it.
if (my_import_array() != 0)
    {
    // Note: in case of error the import_array macro expansion calls
    // PyErr_Print(), then setup another error specification (which
    // we print here too).
    if (PyErr_Occurred() != NULL)
        {
        if (g_print_errors) 
            PyErr_Print();
        else
            PyErr_Clear();
        }
    goto ERROR_IN_INIT;
    }

// Insert numpy as np in our globals.
TRACE_IN("PyImport_ImportModuleEx numpy");
poNumpy = PyImport_ImportModuleEx("numpy", g_poGlobals, g_poGlobals, NULL);
TRACE_OUT("PyImport_ImportModuleEx");

if (poNumpy == NULL)
    {
    fprintf(stderr, "Failure in PyImport_ImportModuleEx() to import numpy\n");
    goto ERROR_IN_INIT;
    }
TRACE_IN("PyDict_SetItemString -> poNumpy ref in g_poGlobals");
PyDict_SetItemString(g_poGlobals, "np",  poNumpy);
TRACE_OUT("PyDict_SetItemString");
Py_CLEAR(poNumpy);

/*TODO: binder éventuellement un module forcallpy avec des fonctions outils,
entre autre s'il y a besoin d'accéder à l'API bas niveau de Fortran pour
allouer des tableaux dynamiques afin de pouvoir ensuite les retourner
en les ayant entre temps associés à des matrices numpy pour les manipuler
en Python.
Si trop difficile, voir si peut faire une fonction du wrapper fortran
qui se charge de l'allocation, et qui est appelée par le wrapper C…
comme ça c'est portable comme on veut!
*/

TRACE_IN("PyImport_ImportModuleEx forcallpy");
poForcallpy = PyImport_ImportModuleEx("forcallpy", g_poGlobals, g_poGlobals, NULL);
TRACE_OUT("PyImport_ImportModuleEx");

if (poForcallpy == NULL)
    {
    fprintf(stderr, "Failure in PyImport_ImportModuleEx() to import forcallpy\n");
    goto ERROR_IN_INIT;
    }

TRACE_IN("PyDict_SetItemString -> forcallpy ref in g_poGlobals");
PyDict_SetItemString(g_poGlobals, "forcallpy",  poForcallpy);
TRACE_OUT("PyDict_SetItemString");



// ===== Finished interpreter initialization.
TRACE_IN("PyEval_SaveThread");
// Memo main thread state structure AND release the GIL.
g_mainstate = PyEval_SaveThread();   // ++++++++++++++++++++++++++++++ -GIL
TRACE_OUT("PyEval_SaveThread");

g_pythonstate = INITIALIZED;        // Change interpreter status.
*coderr = NOERROR;

if (g_verbosity >= VERB_CALLSOUT)
    fprintf(stderr, EXITCALL "forcallpy_init_internal ==> OK\n");
return;

ERROR_IN_INIT: 
TRACE_IN("Py_CLEAR g_poGlobals");
Py_CLEAR(g_poGlobals);              // Release resources.
TRACE_OUT("Py_CLEAR");
TRACE_IN("Py_CLEAR poForcallpy");
Py_CLEAR(poForcallpy);              // Release module.
TRACE_OUT("Py_CLEAR");
TRACE_IN("Py_Finalize");
Py_Finalize();                      // Terminate interpreter.
TRACE_OUT("Py_Finalize");
TRACE_IN("PyMem_RawFree");
PyMem_RawFree(g_program);           // Release lib resources.
TRACE_OUT("PyMem_RawFree");
g_pythonstate = FINALIAZED;         // Change interperter status.
                                    // +++++++++++++++++++++++++ Kept GIL !
                                    
if (g_verbosity >= VERB_CALLSOUT)
    fprintf(stderr, EXITCALL "forcallpy_init_internal ==> ERROR\n");
}


// ========================================================================
/**
 * \brief Call import_array within a function returning and int.
 * 
 * This function is here because import_array is defined as a macro expanding
 * into code which returns an int, not the case of my init function.
 * 
 * \return 0 if ok, else non-zero.
 */
static int
my_import_array(void)
{
// Use variation of import_array macro which allows to specify the return value
// in case of error.
import_array1(-1);
return 0;
}


// ========================================================================
/**
 * \brief Terminate Python stuff of forcallpy library.
 * 
 * \param[out] coderr Integer address to return error code.
 */
void 
forcallpy_terminate_internal(
        int32_t* coderr)
{
assert (coderr != NULL);

*coderr = ERROR_NOT_SET;
    
if (g_verbosity >= VERB_CALLS)
    fprintf(stderr, ENTERCALL "forcallpy_terminate_internal\n");
   

if (g_pythonstate == FINALIAZED)
    {
    *coderr =  NOERROR;
    return;
    }
if (g_pythonstate == UNINITIALIZED)
    {
    fprintf(stderr, "Embedded Python failure: python not initialized, cannot finalize\n");        
    *coderr = PYTHON_NOT_READY;
    return;
    }

g_pythonstate = FINALIAZED;         // Change interperter status.

TRACE_IN("PyEval_RestoreThread");
// Take the GIL for main thread
PyEval_RestoreThread(g_mainstate);  // +++++++++++++++++++++++++++++++ +GIL
TRACE_OUT("PyEval_RestoreThread");

TRACE_IN("Py_CLEAR g_poGlobals");
Py_CLEAR(g_poGlobals);              // Release resources.
TRACE_OUT("Py_CLEAR");

TRACE_IN("Py_Finalize");
Py_Finalize();                      // Terminate interpreter.
TRACE_OUT("Py_Finalize");
// Note: Py_FinalizeEx() is for Python >=3.6
TRACE_IN("PyMem_RawFree g_program");
PyMem_RawFree(g_program);           // Release lib resources.
TRACE_OUT("PyMem_RawFree");
// /!\ Dont release the GIL (interpreter is definitively stopped).
                                    // +++++++++++++++++++++++++ Kept GIL !

*coderr = NOERROR;

if (g_verbosity >= VERB_CALLSOUT)
    fprintf(stderr, EXITCALL "forcallpy_terminate_internal\n");
}


// ========================================================================
/**
 * \brief Build named values to evaluate an expression or call a function.
 * 
 * Parameters from begin of alphabet are integers, parameters from end of alphabet
 * are floating point.
 * Caller must specify its int and real sizes for code adaptation in arrays indexing.
 * 
 * \param[in] intsize Size in bits of an integer (32 or 64).
 * \param[in] itab Array of a-h integer parameter values.
 * \param[in] ipres Array of a-h parameter presence.
 * \param[in] ilen Number of integer parameters (array size).
 * \param[in] realsize Size in bits of a real (32 or 64).
 * \param[in] ftab Array of s-z real parameter values.
 * \param[in] fpres Array of s-z parameter presence.
 * \param[in] flen Number of real parameters (array size).
 * \param[in] poNamespace Borrowed reference to dictionary to fill.
 * \return 0 if ok.
 */
static int 
build_simple_values(
        int intsize,
        void* itab,
        int32_t* ipres,
        int ilen,
        int realsize,
        void* ftab,
        int32_t* fpres,
        int flen,
        PyObject* poNamespace)
{
int errorcode = ERROR_NOT_SET;
int32_t* i32tab = (int32_t*)itab;
int64_t* i64tab = (int64_t*)itab;
float* f32tab = (float*)ftab;
double* f64tab = (double*)ftab;
PyObject* poValue = NULL;
int i;
int ok;

assert (intsize==32 || intsize==64);

if (g_verbosity >= VERB_CALLS)
    fprintf(stderr, ENTERCALL "build_simple_values\n");

// Retrieve providen int (32/64bits) values.
for (i=0 ; i<ilen ; i++)
    {
    if (ipres[i])
        {
        char varname[2];
        varname[0] = 'a' + i;
        varname[1] = 0;
        // Use long long to ensure at least 64 bits in size.
        long long value = (intsize==32?(long long)(i32tab[i]):(long long)(i64tab[i]));

        TRACE_IN("PyLong_FromLongLong -> poValue");
        poValue = PyLong_FromLongLong(value);
        TRACE_OUT("PyLong_FromLongLong");
        if (poValue == NULL) 
            {
            fprintf(stderr, "Failed to get long long value for %s=>%lli\n", varname, value);
            goto ERROR_IN_PYTHON;
            }
        TRACE_IN("PyDict_SetItemString -> poBuiltins ref in poNamespace");
        ok = PyDict_SetItemString(poNamespace, varname,  poValue);
        TRACE_OUT("PyDict_SetItemString");
        if (ok == -1)
            {
            fprintf(stderr, "Failed to store long value for %s=>%lli\n", varname, value);
            goto ERROR_IN_PYTHON;
            }
        Py_CLEAR(poValue);
        }
    }
// Retrieve providen real (float/double) values.
for (i=0 ; i<flen ; i++)
    {
    if (fpres[i])
        {
        char varname[2];
        varname[0] = 'z' - i;
        varname[1] = 0;
        double value = (realsize==32?(double)(f32tab[i]):f64tab[i]);
    
        TRACE_IN("PyFloat_FromDouble -> poValue");
        poValue = PyFloat_FromDouble(value);
        TRACE_OUT("PyFloat_FromDouble");
        if (poValue == NULL) 
            {
            fprintf(stderr, "Failed to get float value for %s=>%f\n", varname, value);
            goto ERROR_IN_PYTHON;
            }
        TRACE_IN("PyDict_SetItemString -> poValue ref in poNamespace");
        ok = PyDict_SetItemString(poNamespace, varname,  poValue);
        TRACE_OUT("PyDict_SetItemString");
        if (ok == -1)
            {
            fprintf(stderr, "Failed to store float value for %s=>%f\n", varname, value);
            goto ERROR_IN_PYTHON;
            }
        Py_CLEAR(poValue);
        }
    }

// That's all right.
errorcode = NOERROR;

ERROR_IN_PYTHON:
if (errorcode == ERROR_NOT_SET) // Oops, something wrong. Cleanup.
    {
    if (PyErr_Occurred() != NULL)
        {
        if (g_print_errors) 
            PyErr_Print();
        else
            PyErr_Clear();
        }
    errorcode = FAILED_PYTHON_CODE;
    }

Py_CLEAR(poValue);

if (g_verbosity >= VERB_CALLSOUT)
    fprintf(stderr, EXITCALL "build_simple_values\n");  

return errorcode;
}     


// ========================================================================
/**
 * \brief Build locals to evaluate an expression.
 * 
 * Parameters from begin of alphabet are integers, parameters from end of alphabet
 * are floating point.
 * Caller must specify its int and real sizes for code adaptation in arrays indexing.
 * 
 * \param[in] intsize Size in bits of an integer (32 or 64).
 * \param[in] iatab Array pointers to integers, av-hv integer arrays parameter values.
 * \param[in] iatab_itemlen Array of length of iatab items.
 * \param[in] ialen Number of integer array parameters (array size).
 * \param[in] realsize Size in bits of a real (32 or 64).
 * \param[in] fatab Array pointers to reals, sv-zv real parameter values.
 * \param[in] fatab_itemlen Array of length of fatab items.
 * \param[in] falen Number of real array parameters (array size).
 * \param[in] readonly Flag to indicate RO or RW arrays.
 * \param[in] poNamespace Borrowed reference to locals dictionary to fill.
 * \return 0 if ok
 */
static int 
build_array_values(
        int intsize,
        void** iatab,
        int32_t* iatab_itemlen,
        int ialen,
        int realsize,
        void** fatab,
        int32_t* fatab_itemlen,
        int falen,
        int readonly,
        PyObject* poNamespace)
{
int errorcode = ERROR_NOT_SET;
PyObject* poValue = NULL;
int i;
int ok;
npy_intp dims[1];
char varname[3];
if (readonly)
    varname[1] = 'v';
else
    varname[1] = 'w';
varname[2] = '\0';

assert (intsize==32 || intsize==64);

if (g_verbosity >= VERB_CALLS)
    fprintf(stderr, ENTERCALL "build_array_values, writable: %d\n", (int)!readonly);

// Retrieve providen int (32/64bits) array values.
for (i=0 ; i<ialen ; i++)
    {
    if (iatab[i] != NULL)    // Ignore null pointers (used as value presence detection flag).
        {
        varname[0] = 'a' + i;
        dims[0] = iatab_itemlen[i];
        // Note: if user provides an empty array, we build an empty array.
    
        if (g_verbosity >= VERB_INTERNAL)
            fprintf(stderr, INTERNVALUE "%s, integers, length %d\n", varname, (int)iatab_itemlen[i]);

        TRACE_IN("PyArray_New -> poValue");
        if (readonly)
            poValue = PyArray_New(&PyArray_Type, 1, dims,  (intsize==32?NPY_INT32:NPY_INT64), 
                              NULL, iatab[i], 0, NPY_ARRAY_IN_FARRAY, NULL);
        else
            poValue = PyArray_New(&PyArray_Type, 1, dims,  (intsize==32?NPY_INT32:NPY_INT64), 
                              NULL, iatab[i], 0, NPY_ARRAY_INOUT_FARRAY, NULL);
        TRACE_OUT("PyArray_New");
        if (poValue == NULL) 
            {
            fprintf(stderr, "Failed to get long array value for %s\n", varname);
            goto ERROR_IN_PYTHON;
            }
        TRACE_IN("PyDict_SetItemString -> poValue ref in poNamespace");
        ok = PyDict_SetItemString(poNamespace, varname,  poValue);
        TRACE_OUT("PyDict_SetItemString");
        if (ok == -1)
            {
            fprintf(stderr, "Failed to store array value for %s\n", varname);
            goto ERROR_IN_PYTHON;
            }
        Py_CLEAR(poValue);
        }
    }
// Retrieve providen real (float/double) array values.
for (i=0 ; i<falen ; i++)
    {
    if (fatab[i] != NULL)    // Ignore null pointers (used as value presence detection flag).
        {
        varname[0] = 'z' - i;
        dims[0] = fatab_itemlen[i];
    
        if (g_verbosity >= VERB_INTERNAL)
            fprintf(stderr, INTERNVALUE "%s, reals, length %d\n", varname, (int)fatab_itemlen[i]);

        TRACE_IN("PyArray_New -> poValue");
        if (readonly)
            poValue = PyArray_New(&PyArray_Type, 1, dims,  (realsize==32?NPY_FLOAT32:NPY_FLOAT64), 
                              NULL, fatab[i], 0, NPY_ARRAY_IN_FARRAY, NULL);
        else
            poValue = PyArray_New(&PyArray_Type, 1, dims,  (realsize==32?NPY_FLOAT32:NPY_FLOAT64), 
                              NULL, fatab[i], 0, NPY_ARRAY_INOUT_FARRAY, NULL);
        TRACE_OUT("PyArray_New");
        if (poValue == NULL) 
            {
            fprintf(stderr, "Failed to get float array value for %s\n", varname);
            goto ERROR_IN_PYTHON;
            }
        TRACE_IN("PyDict_SetItemString -> poValue ref in poNamespace");
        ok = PyDict_SetItemString(poNamespace, varname,  poValue);
        TRACE_OUT("PyDict_SetItemString");
        if (ok == -1)
            {
            fprintf(stderr, "Failed to store float array value for %s\n", varname);
            goto ERROR_IN_PYTHON;
            }
        Py_CLEAR(poValue);
        }
    }

// That's all right.
errorcode = NOERROR;

ERROR_IN_PYTHON:
if (errorcode == ERROR_NOT_SET) // Oops, something wrong. Cleanup.
    {
    if (PyErr_Occurred() != NULL)
        {
        if (g_print_errors) 
            PyErr_Print();
        else
            PyErr_Clear();
        }
    errorcode = FAILED_PYTHON_CODE;
    }

Py_CLEAR(poValue);

if (g_verbosity >= VERB_CALLSOUT)
    fprintf(stderr, EXITCALL "build_array_values\n");  

return errorcode;
}     



// ========================================================================
/**
 * \brief Build locale .
 * 
 * \param[in] patab Array of pointers p parameter value.
 * \param[in] plen Number of pointers in the array.
 * \param[in] readonly Flag to indicate RO or RW arrays.
 * \param[in] poNamespace Borrowed reference to locals dictionary to fill.
 * \return 0 if ok
 */
static int 
build_pointers_value(
        void** ptab,
        int plen,
        int readonly,
        const char* varname,
        PyObject* poNamespace)
{
int errorcode = ERROR_NOT_SET;
PyObject* poValue = NULL;
int ok;
npy_intp dims[1];

if (g_verbosity >= VERB_CALLS)
    fprintf(stderr, ENTERCALL "build_pointers_value, writable: %d\n", (int)!readonly);

if (ptab != NULL)
    {
    dims[0] = plen;
        // Note: if user provides an empty array, we build an empty array.
    
    if (g_verbosity >= VERB_INTERNAL)
        fprintf(stderr, INTERNVALUE "%s, pointers, length %d\n", varname, plen);

    TRACE_IN("PyArray_New -> poValue");
    if (readonly)
        poValue = PyArray_New(&PyArray_Type, 1, dims, NPY_INTP, 
                            NULL, ptab, 0, NPY_ARRAY_IN_FARRAY, NULL);
    else
        poValue = PyArray_New(&PyArray_Type, 1, dims, NPY_INTP, 
                            NULL, ptab, 0, NPY_ARRAY_INOUT_FARRAY, NULL);
    TRACE_OUT("PyArray_New");
    if (poValue == NULL) 
        {
        fprintf(stderr, "Failed to get pointers array for %s\n", varname);
        goto ERROR_IN_PYTHON;
        }
    TRACE_IN("PyDict_SetItemString -> poValue ref in poNamespace");
    ok = PyDict_SetItemString(poNamespace, varname,  poValue);
    TRACE_OUT("PyDict_SetItemString");
    if (ok == -1)
        {
        fprintf(stderr, "Failed to store array value for %s\n", varname);
        goto ERROR_IN_PYTHON;
        }
    Py_CLEAR(poValue);
    }

// That's all right.
errorcode = NOERROR;

ERROR_IN_PYTHON:
if (errorcode == ERROR_NOT_SET) // Oops, something wrong. Cleanup.
    {
    if (PyErr_Occurred() != NULL)
        {
        if (g_print_errors) 
            PyErr_Print();
        else
            PyErr_Clear();
        }
    errorcode = FAILED_PYTHON_CODE;
    }

Py_CLEAR(poValue);

if (g_verbosity >= VERB_CALLSOUT)
    fprintf(stderr, EXITCALL "build_pointers_value\n");  

return errorcode;
}     



// ========================================================================
/**
 * \brief Evaluates Python expression and returns result.
 * 
 * Parameters from begin of alphabet are integers, parameters from end of alphabet
 * are floating point.
 * 
 * Parameter's values are providen to evaluation as local variables with same name, when
 * they have been providen by caller (non null pointers). They can be used where you want
 * in the expression (by example as parameters to a Python function call).
 * 
 * Return value is realized via resptr pointer, exprkind must indicate what kind of
 * value it will contain. In case of underflow/overflow, a value -1 is stored into 
 * resptr referenced memory, and caller must check error code in coderr
 * 
 * \param[in] sexpr Expression to evaluate.
 * \param[in] exprkind Indicator of kind of expression and attended result.
 * \param[in] resptr Place to store the result.
 * \param[in] itab Array of a-h integer parameter values.
 * \param[in] ipres Array of a-h parameter presence.
 * \param[in] ilen Number of integer parameters (array size).
 * \param[in] ftab Array of s-z double parameter values.
 * \param[in] fpres Array of s-z parameter presence.
 * \param[in] flen Number of double parameters (array size).
 * \param[in] ptab Array of p pointers.
 * \param[in] ppres Parameter p presence indicator.
 * \param[in] plen Number of pointers in array.
 * \param[in] iatab Array pointers to integers, av-cv integer arrays ro parameter values.
 * \param[in] iatab_itemlen Array of length of iatab items.
 * \param[in] ialen Number of integer array parameters (array size).
 * \param[in] fatab Array pointers to reals, xv-zv real array ro parameter values.
 * \param[in] fatab_itemlen Array of length of fatab items.
 * \param[in] falen Number of real array parameters (array size).
 * \param[out] iwtab Array pointers to integers, a_-c_ integer arrays rw parameter values.
 * \param[in] iwtab_itemlen Array of length of iatab items.
 * \param[in] iwlen Number of integer array parameters (array size).
 * \param[in] fwtab Array pointers to reals, x_-z_ real array rw parameter values.
 * \param[in] fwtab_itemlen Array of length of fatab items.
 * \param[in] fwlen Number of real array parameters (array size).
 * \param[out] coderr Integer address to store error code.
 */
void 
forcallpy_expr_int32dbl_internal(
        const char* sexpr,
        int32_t exprkind,
        void* resptr,
        int32_t* itab,
        int32_t* ipres,
        int32_t ilen,
        double* ftab,
        int32_t* fpres,
        int32_t flen,
        void** ptab,
        int32_t plen,       
        void** iatab,
        int32_t* iatab_itemlen,
        int ialen,
        void** fatab,
        int32_t* fatab_itemlen,
        int falen,        
        void** iwtab,
        int32_t* iwtab_itemlen,
        int iwlen,
        void** fwtab,
        int32_t* fwtab_itemlen,
        int fwlen,               
        int32_t* coderr)
{
int errorcode = ERROR_NOT_SET;
PyGILState_STATE gilstate;
PyObject* poCallable = NULL;
PyObject* poLocals = NULL;
PyObject* poResult = NULL;
int overflow = 0;
char varname[3];
int i;

// Clear any previous error from some other Python code.
if (PyErr_Occurred()) PyErr_Clear();
assert (coderr != NULL);

if (g_verbosity >= VERB_CALLS)
    fprintf(stderr, ENTERCALL "forcallpy_expr_int32dbl_internal\n");

if (g_verbosity >= VERB_PARAMS_SAMPLE)
    {
    if (exprkind == EXPKIND_RUNSTMT)
        fprintf(stderr, PARAM "sexpr: \n<<<\n%s\n>>>\n", sexpr);
    else
        fprintf(stderr, PARAM "sexpr: %s\n", sexpr);
    }

// For the array of pointers, fortran pass the array pointer address… get the 
// array pointer (we will not modify the pointer itself).
// If someone find a solution in Fortran to pass an optional array of c_ptr
// without this hack…
// Finally, use c_loc(o(1)) and c_loc(p(1)) in Fortran code, this give direct
// access to array components.
//if (ptab) ptab = *ptab;

TRACE_IN("PyGILState_Ensure");
gilstate = PyGILState_Ensure(); // +++++++++++++++++++++++++++++++++++ +GIL
TRACE_OUT("PyGILState_Ensure");
    
/*
// ===== Retrieve callable from expression.
TRACE_IN("PyRun_String -> poCallable");
// Note: MUST use Py_eval_input to retrieve the expression result.
poCallable = PyRun_String(sexpr, Py_eval_input, g_poGlobals, g_poGlobals); // New reference.
TRACE_OUT("PyRun_String");
if (poCallable == NULL) 
    {
    fprintf(stderr, "Failure to retrieve callable from expression\n");
    goto ERROR_IN_PYTHON;
    }
*/
// ===== Build a dictionnary with providen names/values, usable as local environment.
TRACE_IN("PyDict_New -> poLocals");
poLocals  = PyDict_New();
TRACE_OUT("PyDict_New");
if (poLocals == NULL)
    {
    fprintf(stderr, "Failure in PyDict_New() to create locals\n");
    goto ERROR_IN_PYTHON;
    }
TRACE_IN("PyDict_New -> poLocals");

if (build_simple_values(32, (void*)itab, ipres, ilen, 64, (void*)ftab, 
                        fpres, flen, poLocals) != 0)
    {
    fprintf(stderr, "Failure in build_simple_values to fill locals\n");
    goto ERROR_IN_PYTHON;
    }
if (build_pointers_value(ptab, plen, READWRITE, "p", poLocals) != 0)
    {
    fprintf(stderr, "Failure in build_pointers_value to fill locals\n");
    goto ERROR_IN_PYTHON;
    }        
if (build_array_values(32, iatab, iatab_itemlen, ialen, 64, 
                        fatab, fatab_itemlen, falen, READONLY, poLocals) != 0)
    {
    fprintf(stderr, "Failure in build_array_values (ro) to fill locals\n");
    goto ERROR_IN_PYTHON;
    }
if (build_array_values(32, iwtab, iwtab_itemlen, iwlen, 64, 
                        fwtab, fwtab_itemlen, fwlen, READWRITE, poLocals) != 0)
    {
    fprintf(stderr, "Failure in build_array_values (rw) to fill locals\n");
    goto ERROR_IN_PYTHON;
    }
// ===== Call the function.
TRACE_IN("PyRun_String");
if (exprkind == EXPKIND_RUNSTMT) 
    {
    // If we provide a 'locals', then definitions in statements are stored 
    // in these locals. 
    // So we need to mix both to achieve persistence of user definitions
    // in globals.
    PyRun_String(sexpr, Py_file_input, g_poGlobals, poLocals);
    // First remove identifiers used as parameters.
    varname[1] = '\0';
    for (i=0 ; i<SIMPLESIZE ; i++)
        {   
        varname[0] = 'a' + i;
        PyDict_DelItemString(poLocals, varname);
        varname[0] = 'z' - i;
        PyDict_DelItemString(poLocals, varname);
        }
    varname[2] = '\0';
    varname[1] = 'a';
    for (i=0 ; i<ARRAYSIZE ; i++)
        {   
        varname[0] = 'a' + i;
        PyDict_DelItemString(poLocals, varname);
        varname[0] = 'z' - i;
        PyDict_DelItemString(poLocals, varname);
        }
    varname[1] = 'w';
    for (i=0 ; i<ARRAYSIZE ; i++)
        {   
        varname[0] = 'a' + i;
        PyDict_DelItemString(poLocals, varname);
        varname[0] = 'z' - i;
        PyDict_DelItemString(poLocals, varname);
        }
    // Remove KeyError(s).
    PyErr_Clear();
    // Then update dictionnary.
    PyDict_Update(g_poGlobals, poLocals);
    }
else
    poResult = PyRun_String(sexpr, Py_eval_input, g_poGlobals, poLocals); // New reference.
TRACE_OUT("PyRun_String");

if (exprkind != EXPKIND_RUNSTMT && poResult == NULL)
    {
    fprintf(stderr, "Failed to evaluate expression %s\n", sexpr);
    goto ERROR_IN_PYTHON;
    }
else if (exprkind != EXPKIND_RUNSTMT && PyErr_Occurred())
    {
    // With Py_file_input, return of PyRun_String() is all times NULL, 
    // must directly check Python error.
    fprintf(stderr, "Failed to evaluate expressions %s\n", sexpr);
    goto ERROR_IN_PYTHON;
    }
else
    {
    // For result value, we finally require the called function to return a
    // right type value.
    // Conversion errors (by example from an ndarray to a float) are difficult
    // to understand,a clear explanation at that level is better (called
    // expression can have its cast if needed by the developer).
    switch (exprkind)
        {
        case EXPKIND_SUBPROC:               // Consider a procedure, ignore result.
            break;
        case EXPKIND_INT32FCT:
            {
            int32_t result;
            // Note: If poResult is not an instance of PyLongObject, first call its __int__() 
            // method (if present) to convert it to a PyLongObject.
            TRACE_IN("PyLong_AsLongAndOverflow");
            result = (int32_t)PyLong_AsLongAndOverflow(poResult, &overflow);
            TRACE_OUT("PyLong_AsLongAndOverflow");
            *((int32_t*)resptr) = result;
            }
            break;
        case EXPKIND_INT64FCT:
            {
            int64_t result;
            // Note: If poResult is not an instance of PyLongObject, first call its __int__() 
            // method (if present) to convert it to a PyLongObject.
            TRACE_IN("PyLong_AsLongLongAndOverflow");
            result = (int64_t)PyLong_AsLongLongAndOverflow(poResult, &overflow);
            TRACE_OUT("PyLong_AsLongLongAndOverflow");
            *((int64_t*)resptr) = result;
           }
            break;
        case EXPKIND_REAL32FCT:
            {
            float result;
            // Note: If poResult is not an instance of PyLongObject, first call its __int__() 
            // method (if present) to convert it to a PyLongObject.            
            TRACE_IN("PyLong_AsLongAndOverflow");
            result = (float)PyFloat_AsDouble(poResult);
            TRACE_OUT("PyLong_AsLongAndOverflow");
            *((float*)resptr) = result;
            }
            break;
        case EXPKIND_REAL64FCT:
            {
            double result;
            // Note: If poResult is not an instance of PyLongObject, first call its __int__() 
            // method (if present) to convert it to a PyLongObject.            
            TRACE_IN("PyLong_AsLongAndOverflow");
            result = PyFloat_AsDouble(poResult);
            TRACE_OUT("PyLong_AsLongAndOverflow");
            *((double*)resptr) = result;
            }
            break;
        }
    if (overflow != 0 || PyErr_Occurred())
        {
        fprintf(stderr, "Failed to convert expression %s result to required value\n", sexpr);
        goto ERROR_IN_PYTHON;
        }

    }
errorcode = NOERROR;

ERROR_IN_PYTHON:
if (PyErr_Occurred() != NULL)
    {
    if (g_print_errors) 
        PyErr_Print();
    else    
        PyErr_Clear();
    }
if (errorcode == ERROR_NOT_SET)
    {
    errorcode = FAILED_PYTHON_CODE;
    switch (exprkind)
        {
        case EXPKIND_RUNSTMT: break;
        case EXPKIND_SUBPROC: break;
        case EXPKIND_INT32FCT: (*(int32_t*)resptr) = -1; break;
        case EXPKIND_INT64FCT: (*(int64_t*)resptr) = -1; break;
        case EXPKIND_REAL32FCT: (*(float*)resptr) = -1.; break;
        case EXPKIND_REAL64FCT: (*(double*)resptr) = -1.; break;
        }
    }

Py_CLEAR(poResult);
Py_CLEAR(poLocals);
Py_CLEAR(poCallable);

TRACE_IN("PyGILState_Release");
PyGILState_Release(gilstate); // +++++++++++++++++++++++++++++++++++++ -GIL
TRACE_OUT("PyGILState_Release");

*coderr = errorcode;

if (g_verbosity >= VERB_CALLSOUT)
    fprintf(stderr, EXITCALL "forcallpy_expr_int32dbl_internal\n");  
}




// TODO: One function to allocate a fortran POINTER to a dynamic array,
// which can then be stored into an inout p array.
// TODO: One function to retrieve a double* or int64_t* (+dims) from
// a fortran POINTER and build corresponding pynum array.


// ========================================================================
/**
 * \brief Allocate a fortran array referenced by a pointer and return corresponding Numpy array.
 * 
 * For allocation, the function call-back fortran allocation routines
 * This function is called by Python exported allocation dunction.
 * 
 * \param[in,out] ftabpointer Address of fortran ALLOCATABLE or POINTER to fill with allocation.
 * \param[in] bpointer Flag indicating ftabpointer is a POINTER (else its an ALLOCATABLE)
 * \param[in] fcptype Type of fortran array to allocate.
 * \param[in] dim0len Size of array in first dimension.
 * \param[in] dim1len Size of array in second dimension (or <= 0).
 * \param[in] dim2len Size of array in third dimension (or <= 0).
 * \return Address of allocated ndarray
 */
PyObject* 
forcallpy_allocate_array(
        void* ftabpointer, 
        int32_t bpointer,
        int32_t fcptype,
        int32_t dim0len,
        int32_t dim1len,
        int32_t dim2len)
{
int errorcode = ERROR_NOT_SET;
void* allocated = NULL;
PyObject* poArray = NULL;
npy_intp dims[3];
int dimslen;
int typenum;

// Clear any previous error from some other Python code.
if (PyErr_Occurred()) PyErr_Clear();
assert (coderr != NULL);

// Fill dims array.
dims[0] = dim0len;  dimslen = 1;
dims[1] = dim1len; if (dim1len > 0) dimslen = 2;
dims[2] = dim2len; if (dim2len > 0) dimslen = 3;

// Two level switch/case to adapt to ad-hoc Fortran allocation routines
// with type check about POINTER and allocation dimension.
switch (fcptype) 
    {
    case FCP_INT32:
        typenum = NPY_INT32;
        break;
    case FCP_INT64:
        typenum = NPY_INT64;
        break;
    case FCP_REAL32:
        typenum = NPY_FLOAT32;

        break;
    case FCP_REAL64:
        typenum = NPY_FLOAT64;
        if (dim2len > 0)
            {
//            allocated = forcallpy_allocate3d_r8(ftabpointer, dim0len, , dim1len, dim2len, &errorcode);
            }
        else if (dim1len > 0)
            {
//            allocated = forcallpy_allocate2d_r8(ftabpointer, dim0len, dim1len, &errorcode);
            }
        else if (dim0len >= 0)  // Note: allow to allocate 1D empty array
            {
            if (bpointer)
                allocated = c_forcallpy_ptrallocate1d_r8(ftabpointer, dim0len, &errorcode);
            else
                allocated = c_forcallpy_allocate1d_r8(ftabpointer, dim0len, &errorcode);
            }
        else    // Should we allow allocation of "empty" array ?
            {
            fprintf(stderr, "Unsupported type %d for allocation\n", (int)fcptype);        
            goto ERROR_IN_PYTHON;
            }
        break;
    default:
        {
        fprintf(stderr, "Unsupported type %d for allocation\n", (int)fcptype);        
        goto ERROR_IN_PYTHON;
        }
    }
if (allocated == NULL)
    {
    fprintf(stderr, "Failure to allocate FORTRAN array\n");        
    goto ERROR_IN_PYTHON;
    }
poArray = PyArray_New(&PyArray_Type, dimslen, dims,  typenum, 
                NULL, allocated, 0, NPY_ARRAY_IN_FARRAY, NULL);
if (poArray == NULL)
    {
    fprintf(stderr, "Failure to build NumPy ndarray from FORTRAN array\n");        
    goto ERROR_IN_PYTHON;    
    }
return poArray;
    
ERROR_IN_PYTHON:
if (PyErr_Occurred() != NULL)
    {
    if (g_print_errors) 
        PyErr_Print();
    else    
        PyErr_Clear();
    }
return NULL;
}


/**
 * \brief Deallocate a fortran array referenced by a pointer.
 * 
 * For deallocation, the function call-back fortran allocation routines
 * 
 * \param[in,out] ftabpointer Address of fortran POINTER to deallocate.
 * \param[in] bpointer Flag indicating ftabpointer is a POINTER (else its an ALLOCATABLE)
 * \param[in] fcptype Type of fortran array to allocate.
 * \param[in] ndims Number of array dimensions.
 */
void
forcallpy_deallocate_array(
        void* ftabpointer,
        int32_t bpointer,
        int32_t fcptype,
        int32_t ndims)
{
int32_t coderr = 0;

switch (fcptype) 
    {
    case FCP_INT32:
        break;
    case FCP_INT64:
        break;
    case FCP_REAL32:
        break;
    case FCP_REAL64:
        if (ndims == 3)
            {
//            allocated = forcallpy_allocate3d_r8(ftabpointer, dim0len, , dim1len, dim2len, &coderr);
            }
        else if (ndims == 2)
            {
//            allocated = forcallpy_allocate2d_r8(ftabpointer, dim0len, , dim1len, &coderr);
            }
        else if (ndims == 1) 
            {
            c_forcallpy_deallocate1d_r8(ftabpointer, &coderr);
            }
        else    // Should we allow allocation of "empty" array ?
            {
                // Bug ?
            }
        break;
    default:
        {  
        }
    }
}

// ========================================================================
/**
 * \brief Allocate a true fortran array (POINTER), accessible via Numpy ndarray.
 * 
 * Call forcallpy_allocate_array() to allocate the fortran array and associate
 * it with the POINTER, then build the NumPy array corresponding to same
 * memory and shape, and return it.
 * 
 * The newly created NumPy ndarray has light association to data (which address
 * to the given Fortran POINTER).
 * 
 * \param[in,out] faddress Address of corresponding Fortran POINTER/ALLOCTABLE.
 * \param[in] bpointer Boolean indicator we manipulate a POINTER (else its an ALLOCATABLE)
 * \param[in] dtype Numpy data type of data to allocate.
 * \param[in] shape Int or tuple corresponding to array shape.
 * \returns Newly created Numpy ndarray corresponding to Fortran array.
 */
static PyObject*
pyfct_forcallpy_allocate(PyObject *self, PyObject *args)
{
void* faddress; // Use this data type to retrieve pointer.
PyObject* ptrObject = NULL;
int bpointer = 0;
int fcptype = -1;
int dim0len=-1, dim1len=-1, dim2len=-1;
PyObject* npyarray = NULL;

// Clear any previous error from some other Python code.
if (PyErr_Occurred()) PyErr_Clear();

if (g_verbosity >= VERB_CALLS)
    fprintf(stderr, ENTERCALL "pyfct_forcallpy_allocate\n");

// Retrieve parameters from Python tuple.
// Opaque pointer to Fortran POINTER (normally as PyLong)
// Data type as int
// First dimension as int
// Optional second dimension as int
// Optionel third dimension as int
if(!PyArg_ParseTuple(args, "Opii|ii:forcallpy_allocate", &ptrObject, &fcptype,
    &dim0len, &dim1len, &dim2len))
    {
    fprintf(stderr, "Failure to retrieve forcallpy_allocate arguments\n");        
    goto ERROR_IN_PYTHON;
    }
    
faddress = PyLong_AsVoidPtr(ptrObject);
if (faddress == NULL)
    {
    fprintf(stderr, "Failure to retrieve POINTER address\n");        
    goto ERROR_IN_PYTHON;
    }
    
// Call allocation function which route to od-hoc Fortran functions with type checking.
npyarray = forcallpy_allocate_array(faddress, bpointer, fcptype, dim0len, dim1len, dim2len);

return npyarray;
    
ERROR_IN_PYTHON:
if (PyErr_Occurred() != NULL)
    {
    if (g_print_errors) 
        PyErr_Print();
    else    
        PyErr_Clear();
    }
return NULL;
}

// ========================================================================
/**
 * \brief Deallocate a fortran array.
 * 
 * Call forcallpy_deallocate_array() to deallocate the fortran array.
 *
 * \param[in] ndims Int number of dimensions to the array/or correspondng Numpy ndarray
 * \param[in,out] pointer Address of corresponding Fortran POINTER.
 */
static PyObject*
pyfct_forcallpy_deallocate(PyObject *self, PyObject *args)
{
// Clear any previous error from some other Python code.
if (PyErr_Occurred()) PyErr_Clear();

if (g_verbosity >= VERB_CALLS)
    fprintf(stderr, ENTERCALL "pyfct_forcallpy_deallocate\n");



if(!PyArg_ParseTuple(args, "|:forcallpy_deallocate"))
return NULL;
return PyLong_FromLong(0);
}


// ========================================================================
/**
 * \brief Table of Python embeded functions.
 */

static PyMethodDef ForCallPyMethods[] = {
    {"allocate_array", pyfct_forcallpy_allocate, METH_VARARGS,
            "Allocate a fortran array associated to a POINTER, and returns corresponding numpy array."},
    {"deallocate_array", pyfct_forcallpy_deallocate, METH_VARARGS,
            "Deallocate a fortran array associated to a POINTER, from its corresponding numpy array."},
    {NULL, NULL, 0, NULL}
    };
    
    
// ========================================================================
/**
 * \brief Embedded module definition.
 */
static PyModuleDef ForCallPyModule = 
    {
    PyModuleDef_HEAD_INIT, 
    "forcallpy",        // m_name: Module name.
    "Interface to Python/Fortran tool functions.",  // m_doc: Module docs.
    -1,                 // m_size: Dont support subinterpreters.
    ForCallPyMethods,   // m_methods: Table of module level functions.
    NULL,   // m_slots:
    NULL,   // m_traverse:
    NULL,   // m_clear:
    NULL    // m_free:
    };

    
// ========================================================================
/**
 * \brief Embedded module initialisation function.
 */
static PyObject*
pyinit_forcallpy(void)
{
return PyModule_Create(&ForCallPyModule);
}
