Parameters
----------

Functions/subroutines which execute some providen Python code 
(``pyrun()``, ``pysub()``, ``pyfct()``) support
the following optional parameters to transmit data from Fortran to
Python, and in some case back from Python to Fortran.

When evaluating code in the Python interpreter, same names as those
explicitly providen are defined as local variables in the context 
of the evaluation, and can be directly used.

The use of identified names allows to directly know data types in Fortran
and Python codes. Arrays are transmitted via `numpy`_ type `ndarray`_ objects, 
which are used as wrappers directly accessing Fortran arrays storage
(there is no data copy).
Arrays providen as ``in`` parameter are mapped to read-only ndarrays
(numpy ensures that data are not modified).
Arrays providen as ``inout`` parameter are mapped to read-write ndarrays.
Names with single values may be re-affected within Python code, this will have 
no effect on the Fortran side (this is why they are noted read only).

Names at begin of the alphabet correspond to Integers, names at end of the
alphabet to Reals. Two char names ending with a ``v`` are read-only one 
dimension arrays (or *vectors*), 
and those ending with a ``w`` are one dimension read-write arrays
(or *write vectors*). 


.. _numpy: http://www.numpy.org/
.. _ndarray: https://docs.scipy.org/doc/numpy-1.13.0/reference/generated/numpy.ndarray.html

======================= =============== =============== =======================
Names                   Fortran         Python          Direction
======================= =============== =============== =======================
``a`` to ``h``          Integer         int             in (read only)
``s`` to ``z``          Real            float           in (read only)
``av`` to ``cv``        Integer(:)      ndarray         in (read only)
``xv`` to ``zv``        Real(:)         ndarray         in (read only)
``aw`` to ``cw``        Integer(:)      ndarray         inout (read write)
``xw`` to ``zw``        Real(:)         ndarray         inout (read write)
``p``                   c_ptr(:)        ndarray         inout (read write)
======================= =============== =============== =======================

Depending on API functions suffix, Integer can be 4 bytes or 8 bytes int 
(``i4`` or ``i8``), and Real 4 bytes or 8 bytes float (``r4`` or ``r8``).
Numpy's ``ndarray`` are directly mapped to corresponding types sizes
(no data copy). We dont support mixing of different Integer sizes or different
Real sizes : all paramaters of a type have same size.

.. note:: 

    currently only 4 bytes int and 8 bytes float (``i4r8``) is coded 
    in the library, but adding other variations is easy.

These multiple definitions or subroutines and functions allows to have correct type 
checking when calling Python wrapper functions, and help construction of numpy's 
ndarrays to correctly wrap Fortran arrays.

The ``p`` parameter is a placeholder array to exchange some memory addresses, 
by example to transmit unsupported data, or to allow direct modification of a 
Fortran POINTER by the Python code.
