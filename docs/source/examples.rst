Examples
--------

.. comment:
    http://www.sphinx-doc.org/en/stable/markup/code.html
    :target: un_essai

Note : you can find these examples in ``demo/forcallpy_demo.f90`` Fortran program, 
coming with ``demo/forcallpy_demomodule.py`` Python module.

The Fortran code is written using Fortran 2003 syntax and language tools. 
You should compile your program using at least that version of the language
(still using extension ``.f90`` for files).

For adaptation to different Fortran Integer and Real sizes, functions which
transmit data are postfixed with data size indication for parameters and
if necessary for return value. 
For examples we will use ``_i4r8`` (Integer 4 bytes and Real 8 bytes).

Interpreter begin and halt
~~~~~~~~~~~~~~~~~~~~~~~~~~

You will have to explicitly start and stop the Python interpreter from your Fortran code
using subroutines ``pyinit()`` and ``pyterm()``.
The interpreter can effectively start only one time in your program, 
once stopped it cannot be restarted.
The ``pyinit()`` subroutine can be called several times to change some general
running options (typically verbosity, error processing…). 

.. code-block:: fortran

    USE forcallpy
    USE, INTRINSIC :: iso_c_binding, ONLY: c_ptr, c_null_ptr, c_loc
    CALL pyinit()
    !
    !
    ! Here you can use the interpreter (here takes place following examples).
    !
    !
    CALL pyterm()

As you can see, from Fortran 2003 iso C bindings, we import several names that may be used to
define data which can be exchanged with Python code.

When starting the interpreter, the global namespace used in further calls
to statements execution or expressions evaluations is automatically filled 
with names:

* ``__builtins__`` providing access to standard Python names,
* ``np`` alias for ``numpy`` package which is imported, 
* ``forcallpy`` (internal C module) providing tools for Python code interaction
  with Fortran — typically to allocate and manipulate dynamic Fortran arrays 
  on the Python side.

The Python Interpreter within your Fortran process normally uses environment variables
to retrieve Python related ones. 
Particularly the `PYTHONPATH environment variable`_ which lists complement paths
where Python must search when importing modules or packages.

.. _PYTHONPATH environment variable: https://docs.python.org/3/using/cmdline.html#envvar-PYTHONPATH


Here are data definitions for following examples:

.. code-block:: fortran

    INTEGER :: coderr
    INTEGER :: intres
    INTEGER,DIMENSION(10),TARGET :: tabint;
    DOUBLE PRECISION,DIMENSION(5),TARGET :: tabdbl;
    TYPE(c_ptr),DIMENSION(3) :: tabptr;
    DOUBLE PRECISION,DIMENSION(4),TARGET :: tabres;

    tabint(1:10) = (/ 3, 4, 5, 6, 7, 8, 9, 10, 11, 12  /)
    tabdbl(1:5) = (/ 10.1, 10.3, 10.5, 10.7, 10.9 /)
    tabptr(1:3) = (/ c_null_ptr, c_null_ptr , c_null_ptr  /)
    tabres(1:4) = (/ 0.0, 0.0, 0.0, 0.0 /)
    tabptr(2) = c_loc(tabdbl)


Running statements
~~~~~~~~~~~~~~~~~~
    
The library provides a ``pyrun()`` subroutine for that. It can execute multiple statements
of Python code, import modules or packages, define variables / functions / classes, etc.
The Python code is simply providen as first parameter (string) to the subroutine.
    
.. code-block:: fortran
   
    CALL pyrun_i4r8('for i in range(3): print("This is a Python loop", i, "directly in fortran source.")')
    CALL pyrun_i4r8('import math')
    CALL pyrun_i4r8('print("Pi value is", math.pi)')
    CALL pyrun_i4r8('import sys'//NEW_LINE('a')//'sys.path.insert(0, ".")')

Modifications made to global environment via ``pyrun()`` (names imported or defined…) are memorized 
and made available for furthers calls to the Python interpreter.

When calling the Python interpreter within Fortran, you can provide some data via 
specific named parameters of Fortran functions/subroutines, which are made available 
as local variables in Python when evaluating statements and expressions
(these variables names are excluded from memorization in the globals namespace 
between different calls, you must choose other names for persistance or use another
Python namespace).

.. code-block:: fortran

    CALL pyrun_i4r8('print("result =",3 + 4 *  x)', x=3.6D0)
   
One statement
~~~~~~~~~~~~~

To just execute a single statement, like a subroutine call, you can use ``pycall()`` subroutine,
it works like ``pyrun()`` but with limitations. It must be seen as a Fortran ``CALL``
statement to a Python callable (function, object, class…).
Here again, Python code is first parameter (string).


The example is used to transmit arrays to the subroutine.

.. code-block:: fortran

    CALL pyrun_i4r8('import forcallpy_demomodule')
    CALL pysub_i4r8("forcallpy_demomodule.a_subroutine(av,zv,p)", &
                                    av=tabint,zv=tabdbl,p=tabptr)      

Calling Python function
~~~~~~~~~~~~~~~~~~~~~~~

This is more general than simply calling a function, you can execute any Python 
expression which produces a result, including a call to some function or any 
callable (class, object method, callable object…).
You have just to use ``pyfct()`` function which returns a result. 
It has a complement suffix to specify the attended type of the result 
(Integers ``_i4`` or ``_i8``, Reals ``_r4`` or ``_r8``, ``_bool`` for Fortran LOGICAL).
Python code is still first parameter (string).

In this example, not only we provide in arrays ``av`` and ``zv``, 
but also one inout array ``yw`` which is modified in the called function.

.. code-block:: fortran

    intres = pyfct_i4r8_i4("int(forcallpy_demomodule.a_function(a,b,c,x,av,zv,yw))", &
                                    a=2,b=-4,c=7,x=3.5D+0,av=tabint,zv=tabdbl,yw=tabres)
