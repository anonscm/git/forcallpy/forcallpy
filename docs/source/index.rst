.. forcallpy documentation master file, created by
   sphinx-quickstart on Fri Oct 20 09:44:30 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Forcallpy's documentation
=========================

.. comment:
    Contents:

    .. toctree::
        :maxdepth: 2



.. include:: about.rst

.. include:: examples.rst

.. include:: parameters.rst

.. include:: apirefs.rst

.. include:: building.rst



.. comment:
    Indices and tables
    ==================
    
    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`

