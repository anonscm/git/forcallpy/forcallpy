About
-----

The common way to mix `Fortran`_ and `Python`_ languages is to build Fortran 
functions/subroutines and to generate wrapping modules with `f2py`_.
You have then just to import these (natively compiled) modules from your 
Python code, its *automagic*.

.. _Fortran: https://en.wikipedia.org/wiki/Fortran
.. _Python: https://www.python.org/
.. _f2py: https://docs.scipy.org/doc/numpy-dev/f2py/

For a project, we needed to interact in the other way: the Fortran code
containing the main program should be able to call some Python
functions, providing data as parameters and retrieving results.

As Python not only can be extended with C dynamic libraries, but can also
be embedded into C programs using its internal C API, and Fortran 
(especially since Fortran 2003) has language defined interfaces with C,
we decided to embed Python within Fortran *via* some C glue code. 

This is the job of this ``forcallpy`` library : embed a Python interpreter
within your Fortran program, allow to call Python code and manage 
Fortran/Python data exchange. All this with secure type checking and
less possible data duplication.


License
~~~~~~~

This library is providen under the CeCILL-B License, which is compatible
with BSD + author citation licenses. You can find the license inside sources
repository, or directly inline on `cecill.info site`_.

.. _cecill.info site: http://www.cecill.info/licences/Licence_CeCILL-B_V1-en.html


Code
~~~~

Even without Fortran, the C part of this library (file ``forcallpy_cwrapper.c``)
can be used as an example of Python embedding, 
with support of Python multithreading, C level use of Python well know ``numpy`` library
and definition of an internal C module. 

Some of these developments required further investigations in the WEB to achieve
a working library, comments in the code can help to achieve a similar integration
in another context.
