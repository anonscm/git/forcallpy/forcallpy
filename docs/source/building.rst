Building
--------
 
Notes about library construction.

It uses a  C compiler (…gcc), a Fortran compiler (…gfortran) and a cPython installation
with numpy (use Python libs and numpy C header).

Source directory contains a ``manualbuild.sh`` script, I initially used to build the library
(kept here if you need to make your own build).

Build process switched to **cmake**, with some constraint (multiple languages, integration of
version in produced library, variation in location of numpy headers… and long time to fix some 
weird results).

Download
~~~~~~~~

The project repository is available on SourceSup (french academic code repository), just
retrieve it::

    git clone https://git.renater.fr/forcallpy.git path_to_source
    
Build
~~~~~

You need to have  `cmake`_ installed for your platform. Create a build directory somewhere,
and ask cmake to create build files. Then use your plaftorm build system to compile and
link the library (example on Linux with GNU make toolchain)::

    me@host:~…/build$ cmake path_to_source
    me@host:~…/build$ make

.. _cmake: https://cmake.org/


Build with anaconda
~~~~~~~~~~~~~~~~~~~

.. note::
    Examples are given using Anaconda on Linux, they must be adapted to your system.
    
    Currently, using Anaconda compilers on MacOS failed due to some Fortran 2003 
    option used in the source (C/Fortran integration) and not available in the compiler 
    on that system. You may try to use macports (see receipt below).
    
    Note: This has not yet been tested on Windows platform.

    
Install `Anaconda compilers`_::

  $ conda install -c anaconda gcc_linux-64
  $ conda install -c anaconda gfortran_linux-64

Request CMake to use these compilers (else it will search for them in standard paths), it should 
use Anaconda Python installation found in path. 
Be careful with env variable names, their case must be exact (typically: ``CMAKE_Fortran_COMPILER``)::

  me@host:~…/build$ cmake path_to_source -DCMAKE_C_COMPILER=x86_64-conda_cos6-linux-gnu-gcc -DCMAKE_Fortran_COMPILER=x86_64-conda_cos6-linux-gnu-gfortran
  
.. _Anaconda compilers: https://conda.io/docs/user-guide/tasks/build-packages/compiler-tools.html

.. 
    noteabout **Make verbosity**

    To  systematically enable make verbosity, generate makefiles with giving option
    ``-DCMAKE_VERBOSE_MAKEFILE=ON`` at cmake invocation.
   
    To enable make verbosity just once, use ``make VERBOSE=1``.
   
    This is used to check what tools are really used  within make compilation/link chain.


Build with MacPorts
~~~~~~~~~~~~~~~~~~~

You must install `MacPorts`_ for your MacOSX version (it will require installation of XCode). 
See installation guides on MacPorts site (don't miss to install cli dev tools of XCode and
to agree xcodebuild licence).

.. _MacPorts: https://www.macports.org/

Once MacPorts is ready, you can install ``gcc7``, ``python36``, ``py36-numpy`` and also ``cmake``
(the standard MacOSX bundled CMake dont find Python libraries from MacPorts)::

    port search --name 'gcc*'
    sudo port install gcc7
    hash -r
    port select --list gcc
    sudo port select --set gcc mp-gcc7
    port search --name 'python3*'
    sudo port install python36
    hash -r
    sudo port select --set python3 python36
    port search --name '*numpy*'
    sudo port install py36-numpy
    port search --name 'cmake*'
    sudo port install cmake
    hash -r

Then, you can go back to the standard previous `Build`_ procedure.


Test
~~~~

To execute the demo test program (built with library), you must cd to demo source
directory (to have Python demo scripts available) and execute the demo binary from
this directory::

    me@host:~…/build$ cd path_to_source/demo
    me@host:path_to_source/demo$ ~…/build/forcallpy_demo

This should produce the following result::

    result = 17.4
    This is a Python loop 0 directly in fortran source.
    This is a Python loop 1 directly in fortran source.
    This is a Python loop 2 directly in fortran source.
    Pi value is 3.141592653589793
    Begin of module forcallpy_testmodule.py initialization
    Pi is 3.141592653589793
    A python loop 0 in a module.
    A python loop 1 in a module.
    A python loop 2 in a module.
    End of module forcallpy_testmodule.py initialization
    Doc forcallpy: Interface to Python/Fortran tool functions.
    Inout array before:   0.0000000000000000        0.0000000000000000        0.0000000000000000        0.0000000000000000     
    Called a_int_function() with:
        a,b,c,x: 2 -4 7 3.5
        vint: [ 3  4  5  6  7  8  9 10 11 12]
        vfloat: [ 10.10000038  10.30000019  10.5         10.69999981  10.89999962]
        resfloats: [ 0.  0.  0.  0.]
        types vint,vfloat,resfloats: <class 'numpy.ndarray'> <class 'numpy.ndarray'> <class 'numpy.ndarray'>
        np.sin of our float array: [-0.62507095 -0.76768593 -0.87969576 -0.95663496 -0.99543622]
        Modifying resfloats array with values of int squares
        -> Returning: 17.5
    Result =          17
    Inout array after:   1.0000000000000000        4.0000000000000000        9.0000000000000000        16.000000000000000     
    Called a_subroutine() with:
    Integers [ 3  4  5  6  7  8  9 10 11 12]
    Floats [ 10.10000038  10.30000019  10.5         10.69999981  10.89999962]
    Pointers [              0 140732863141376               0]
    Types: <class 'numpy.ndarray'> <class 'numpy.ndarray'> <class 'numpy.ndarray'>
    Computing a zero division in Python
    Failed to evaluate expression print(1/0)
    Traceback (most recent call last):
      File "<string>", line 1, in <module>
    ZeroDivisionError: division by zero
    Multithreading in Python...
    begin thread 139919767045888 1526280602.4691195
    begin thread 139919758653184 1526280602.4692402
    begin thread 139919750260480 1526280602.4693658
    ...Threads started
    end thread 139919767045888 1526280604.4701622
    end thread 139919758653184 1526280606.4715772
    end thread 139919750260480 1526280608.4754772
